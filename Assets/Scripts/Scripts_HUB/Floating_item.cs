﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating_item : MonoBehaviour {
	private bool changeDirection = true;

	void Update () {

		if(this.changeDirection==true){
			transform.Translate (new Vector3(0,6,0)*Time.deltaTime);
			if(transform.position.y>-130f){
				this.changeDirection = false;
			}
		}

		if(this.changeDirection==false){
			transform.Translate (new Vector3(0,-6,0)*Time.deltaTime);
			if(transform.position.y<-135f){
				this.changeDirection = true;
			}
		}

	}
}
