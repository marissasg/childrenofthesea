﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUBMove : MonoBehaviour {

	Rigidbody RB;
	private float PlayerSpeed;

	//Parámetros para el salto 
	bool grounded = false; 
	Collider[] groundCollisions;
	float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	public Transform groundCheck;
	public float jumpHeight;

	//Variable para usar las animaciones
	private Animator anim;

	// Use this for initialization
	void Start () {
		RB = GetComponent<Rigidbody> ();
		this.anim = this.GetComponent<Animator> ();
		if(PlayerPrefs.GetInt("CharacterID")==5) {
			this.PlayerSpeed = 5.0f;
		}else {
			this.PlayerSpeed = 3.5f;
		}
		//anim.SetBool ("isInHUB", true);
	}
	
	// Update is called once per frame
	void Update () {
		//Salto
		if (grounded && Input.GetButton ("Jump")) {
			grounded = false;
			anim.SetBool ("Grounded", grounded);
			//anim.SetFloat("verticalSpeed", RB.velocity.y);
			RB.AddForce (new Vector3 (0, jumpHeight, 0));
		}

		groundCollisions = Physics.OverlapSphere (groundCheck.position, groundCheckRadius, groundLayer);
		if (groundCollisions.Length > 0) {
			grounded = true;

		} else {
			grounded = false;
		}

		anim.SetBool ("isInHUB", false);
		anim.SetBool ("isInMaze",false);
		anim.SetBool ("isSeq", false);
		anim.SetBool ("ItLost", false);
		anim.SetBool ("ItWin", false);
		anim.SetBool ("Grounded", grounded);
		anim.SetFloat("verticalSpeed", RB.velocity.y);

		//Movimiento de joystick
		float move = (Input.GetAxis ("Horizontal")) + (Input.GetAxis("Vertical"));
		anim.SetFloat ("speed", Mathf.Abs (move));

		//Movimiento segun la camara
		Vector3 moveVector = Vector3.zero;
		//Camera.main.transform.forward;

		if (Input.GetAxis ("Horizontal") > 0) { //Derecha
			Vector3 targetPoint = new Vector3 (90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint, Vector3.up);
			var targetRotCAM = Quaternion.LookRotation (Camera.main.transform.right, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotCAM ,0.2f);
			transform.Translate (Camera.main.transform.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
		}
		if (Input.GetAxis ("Horizontal") < 0) { //Izquierda
			Vector3 targetPoint = new Vector3 (-90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint, Vector3.up);
			var targetRotCAM = Quaternion.LookRotation (-Camera.main.transform.right, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotCAM ,0.2f);
			transform.Translate (Camera.main.transform.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
		}
		if (Input.GetAxis ("Vertical") > 0) { //Arriba
			Vector3 targetPoint = new Vector3 (0f, 0f, 90f);
			var targetRot = Quaternion.LookRotation (targetPoint, Vector3.up);
			var targetRotCAM = Quaternion.LookRotation (Camera.main.transform.forward, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotCAM ,0.2f);
			transform.Translate (Camera.main.transform.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
		}
		if (Input.GetAxis ("Vertical") < 0) { //Abajo
			Vector3 targetPoint = new Vector3 (0f, 0f, -90f);
			var targetRot = Quaternion.LookRotation (targetPoint, Vector3.up);
			var targetRotCAM = Quaternion.LookRotation (-Camera.main.transform.forward, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotCAM ,0.2f);
			transform.Translate (Camera.main.transform.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
		}
	}
}
