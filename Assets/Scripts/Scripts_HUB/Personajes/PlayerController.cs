﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private CharacterController controller;

	private float distance = 5.0f;

	//Variable para usar las animaciones
	private Animator anim;

	//Velocidad 
	public float PlayerSpeed = 20.0f;

	//Variables para el salto 
	private float verticalVelocity;
	private float gravity = 15.0f;
	private float jumpForce = 8.0f;

	// Use this for initialization
	void Start () {
		this.anim = this.GetComponent<Animator> ();
		controller = GetComponent<CharacterController> ();
		anim.SetBool ("isInHUB", true);
	}

	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(transform.position), 0f);
		if (controller.isGrounded) {
			verticalVelocity = -gravity * Time.deltaTime;
			if (Input.GetButtonDown ("Jump")) {
				verticalVelocity = jumpForce;
				/*this.anim.Play ("Default_Jump");
				this.anim.Play ("Zombie_Jump");
				this.anim.Play ("Bunny_Jump");
				this.anim.Play ("Puff_Jump");
				this.anim.Play ("Dragon_Jump");
				this.anim.Play ("Raccstar_Jump");
				this.anim.Play ("Picatsso_Jump");*/
				}
			} else {
					verticalVelocity -= gravity * Time.deltaTime;
					}
		Vector3 moveVector = Vector3.zero;
		moveVector.x = Input.GetAxisRaw ("Horizontal") * distance;
		moveVector.y = verticalVelocity;
		moveVector.z = Input.GetAxisRaw ("Vertical") * distance;
		moveVector = Camera.main.transform.TransformDirection (moveVector);
		//transform.rotation = Quaternion.LookRotation (moveVector);
		//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveVector), 0.15f);
		controller.Move (moveVector * Time.deltaTime);
		if (Input.GetAxis ("Horizontal") == 0 || Input.GetAxis ("Vertical") == 0) {

			/*this.anim.Play ("Default_Idle");
			this.anim.Play ("Zombie_Idle");
			this.anim.Play ("Bunny_Idle");
			this.anim.Play ("Puff_Idle");
			this.anim.Play ("Dragon_Idle");
			this.anim.Play ("Raccstar_Idle");
			this.anim.Play ("Picatsso_Idle");*/

		} else if (Input.GetAxis ("Horizontal") != 0) {
			float moveX = Input.GetAxis ("Horizontal");
			anim.SetFloat ("speed", Mathf.Abs (moveX));
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (moveVector), 0.15f);

			/*this.anim.Play ("Default_Walk");
			this.anim.Play ("Zombie_Walk");
			this.anim.Play ("Bunny_Walk");
			this.anim.Play ("Puff_Walk");
			this.anim.Play ("Dragon_Run");
			this.anim.Play ("Raccstar_Walk");
			this.anim.Play ("Picatsso_Walk");*/
		} else if (Input.GetAxis ("Vertical") != 0) {
			float moveForward = Input.GetAxis ("Vertical");
			anim.SetFloat ("speed", Mathf.Abs (moveForward));
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (moveVector), 0.15f);

		}
	}
}
