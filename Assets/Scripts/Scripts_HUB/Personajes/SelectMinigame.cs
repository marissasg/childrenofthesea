﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectMinigame : MonoBehaviour {
	
	public Image white;
	public Animator anim;

	// Use this for initialization
	void Start () {
		
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.name == "Col_maze") {
			StartCoroutine(Fading ());
			SceneManager.LoadScene ("Ins_maze");
		}
		if (other.gameObject.name == "Col_platforms") {
			SceneManager.LoadScene ("Ins_plat");
			StartCoroutine(Fading ());

		}
		if (other.gameObject.name == "Col_sequence") {
			StartCoroutine(Fading ());
			SceneManager.LoadScene ("Ins_seq");
		}
		if (other.gameObject.name == "Col_color") {
			StartCoroutine(Fading ());
			SceneManager.LoadScene ("Ins_color");
		}
		if (other.gameObject.name == "Col_credits") {
			StartCoroutine(Fading ());
			SceneManager.LoadScene ("Credits");
		}
		if (other.gameObject.name == "Col_shop") {
			StartCoroutine(Fading ());
			SceneManager.LoadScene ("Shop");
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
