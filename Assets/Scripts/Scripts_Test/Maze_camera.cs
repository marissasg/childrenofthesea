﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze_camera : MonoBehaviour {

	[SerializeField]
	private float xMax, zMax, xMin, zMin;

	private Transform target;

	void Start () {
		target = GameObject.Find ("Player").transform;
	}

	void LateUpdate () {
		transform.position = new Vector3 (Mathf.Clamp(target.position.x, xMin, xMax), 8.7f, Mathf.Clamp(target.position.z, zMin, zMax));
	}
}
