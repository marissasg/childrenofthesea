﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gems_audio_maze : MonoBehaviour {

	public AudioClip[] audioClip;
	private AudioSource source;

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	
	void OnTriggerEnter(Collider col){
		playSound(0);
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}
}
