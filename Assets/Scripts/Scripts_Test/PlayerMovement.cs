﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	//Velocidad 
	public float PlayerSpeed = 20.0f;

	//Variables para el salto 
	private float verticalVelocity;
	private float gravity = 14.0f;
	private float jumpForce = 10.0f;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.name == "Cube_Piso") { //CONDICION MIENTRAS TOCA EL SUELO QUE DEBE SER EN EL SCRIPT DE PLAYER MOVEMENT
			print ("CHOCO CON EL SUELO");
			verticalVelocity = -gravity * Time.deltaTime;
			if (Input.GetButtonDown ("Jump")) {
				print ("ESTA SALTANDO");
				verticalVelocity = jumpForce;
			} 

		}else {
				verticalVelocity -= gravity * Time.deltaTime;
			}
			Vector3 moveVector = Vector3.zero;
			moveVector.x = Input.GetAxis ("Horizontal") * 5.0f; 
			moveVector.y = verticalVelocity;
			//moveVector.z = Input.GetAxis ("Vertical") * 5.0f;
			transform.Translate (moveVector * Time.deltaTime);
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime);
		transform.Translate (Vector3.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime);
	}
		
}
