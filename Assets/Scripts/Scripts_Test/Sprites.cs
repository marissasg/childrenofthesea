﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprites : MonoBehaviour {

	public bool newSequence;
	public bool refreshImage;

	public List<int> gameSequence = new List<int> ();
	private List<int> inputSequence = new List<int> ();

	private int character=5;

	//0 es A	4 es arriba
	//1 es B	5 es abajo
	//2 es X	6 es izquierda
	//3 es Y	7 es derecha


	void Start () {
		//this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_A;

		//	AQUI HACER UNA CONDICION DE QUE SI ES PUFF CHARACTER ES 3 Y SI NO , CHARACTER ES 5  
		//		Y QUITAR DE ARRIBA EN LA DECLARACION EL NUMERO DE LA VARIABLE

		this.newSequence = false;
		this.refreshImage = false;

		if(this.character==3){
			for(int i=0 ; i < 5 ; i++){
				if(i<3){
					gameSequence.Add (Random.Range (0,8));
				}
				if(i==3){
					gameSequence.Add (8);
				}
				if(i==4){
					gameSequence.Add (8);
				}

			}
		}
		if(this.character==5){
			for(int i=0 ; i < 5 ; i++){
				gameSequence.Add (Random.Range (0,8));
			}
		}

		print ("NUEVA secuencia real");
		for(int i=0 ; i < character ; i++){
			print (gameSequence[i]);
		}

		//this.spritesCreated = true;
	}
		

	void Update () {//<>

		#region IF KEY DOWN 0 AL 7

		if(Input.GetKeyDown(KeyCode.A)){
			//print ("Agregue a INPUT 0");
			inputSequence.Add (0);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.B)){
			//print ("Agregue a INPUT 1");
			inputSequence.Add (1);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.X)){
			//print ("Agregue a INPUT 2");
			inputSequence.Add (2);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.Y)){
			//print ("Agregue a INPUT 3");
			inputSequence.Add (3);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			//print ("Agregue a INPUT 4");
			inputSequence.Add (4);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.DownArrow)){
			//print ("Agregue a INPUT 5");
			inputSequence.Add (5);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			//print ("Agregue a INPUT 6");
			inputSequence.Add (6);
			this.refreshImage=false;
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			//print ("Agregue a INPUT 7");
			inputSequence.Add (7);
			this.refreshImage=false;
		}

		#endregion

		#region CREAR NUEVA SECUENCIA
		if(newSequence==true){
		
		inputSequence.Clear();
		gameSequence.Clear();

			if(this.character==3){
				for(int i=0 ; i < 5 ; i++){
					if(i<3){
						gameSequence.Add (Random.Range (0,8));
					}
					if(i==3){
						gameSequence.Add (8);
					}
					if(i==4){
						gameSequence.Add (8);
					}

				}
			}
			if(this.character==5){
				for(int i=0 ; i < 5 ; i++){
					gameSequence.Add (Random.Range (0,8));
				}
			}

			print ("NUEVA secuencia real ");
			for(int i=0 ; i < character ; i++){
				print (gameSequence[i]);
			}
		
			this.refreshImage=true;
			this.newSequence=false;


			//print ("refresh image es "+refreshImage);

		}

		#endregion

		#region COMPROBAR SENCUENCIA REAL CON INPUT

		if(inputSequence.Count==this.character){

			bool gameOver=false;

			if(this.character==3){
				for(int i=0 ; i < 3 ; i++){
					if(gameOver==false){
						if(inputSequence[i]!=gameSequence[i]){
							gameOver=true;
							print("PERDEDOR");
						}
					}
				}
				if(gameOver==false){
					print("GANADOR");
				}

			}else{
				for(int i=0 ; i < gameSequence.Count ; i++){
					if(gameOver==false){
						if(inputSequence[i]!=gameSequence[i]){
							gameOver=true;
							print("PERDEDOR");
						}
					}
				}
				if(gameOver==false){
					print("GANADOR");
				}
				
			}




			newSequence=true;

		}
		#endregion
					
	}


}
