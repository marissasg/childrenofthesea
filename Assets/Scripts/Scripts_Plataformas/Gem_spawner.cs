﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem_spawner : MonoBehaviour {

	public GameObject gemBag, gemZombie, gemBun, gemPuff, gemDragon, gemCat;
	public GameObject sparksBag, sparksZombie, sparksBun, sparksPuff, sparksDragon, sparksCat;
	private Vector3 gem01, gem02, gem03, gem04, gem05;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

		/*
	 * 1	Bag
	 * 2	Zombie
	 * 3	Bunn
	 * 4	Puff
	 * 5	Dragon
	 */

	void Start () {

		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		this.gem01 = new Vector3 (-9.13f, 10.04f, 0f);
		this.gem02 = new Vector3 (-9.11f, 21.83f, 0f);
		this.gem03 = new Vector3 (8.94f, 31.12f, 0f);
		this.gem04 = new Vector3 (10.03f, 46.94f, 0f);
		this.gem05 = new Vector3 (9.1f, 12.18f, 0f);

		if(PlayerPrefs.GetInt("CharacterID") == 1){
			Instantiate (this.gemBag, this.gem01, Quaternion.identity);
			Instantiate (this.gemBag, this.gem02, Quaternion.identity);
			Instantiate (this.gemBag, this.gem03, Quaternion.identity);
			Instantiate (this.gemBag, this.gem04, Quaternion.identity);
			Instantiate (this.gemBag, this.gem05, Quaternion.identity);

			Instantiate (this.sparksBag, this.gem01, Quaternion.identity);
			Instantiate (this.sparksBag, this.gem02, Quaternion.identity);
			Instantiate (this.sparksBag, this.gem03, Quaternion.identity);
			Instantiate (this.sparksBag, this.gem04, Quaternion.identity);
			Instantiate (this.sparksBag, this.gem05, Quaternion.identity);
		}
		if(PlayerPrefs.GetInt("CharacterID") == 2){
			Instantiate (this.gemZombie, this.gem01, Quaternion.identity);
			Instantiate (this.gemZombie, this.gem02, Quaternion.identity);
			Instantiate (this.gemZombie, this.gem03, Quaternion.identity);
			Instantiate (this.gemZombie, this.gem04, Quaternion.identity);
			Instantiate (this.gemZombie, this.gem05, Quaternion.identity);

			Instantiate (this.sparksZombie, this.gem01, Quaternion.identity);
			Instantiate (this.sparksZombie, this.gem02, Quaternion.identity);
			Instantiate (this.sparksZombie, this.gem03, Quaternion.identity);
			Instantiate (this.sparksZombie, this.gem04, Quaternion.identity);
			Instantiate (this.sparksZombie, this.gem05, Quaternion.identity);
		}
		if(PlayerPrefs.GetInt("CharacterID") == 3){
			Instantiate (this.gemBun, this.gem01, Quaternion.identity);
			Instantiate (this.gemBun, this.gem02, Quaternion.identity);
			Instantiate (this.gemBun, this.gem03, Quaternion.identity);
			Instantiate (this.gemBun, this.gem04, Quaternion.identity);
			Instantiate (this.gemBun, this.gem05, Quaternion.identity);

			Instantiate (this.sparksBun, this.gem01, Quaternion.identity);
			Instantiate (this.sparksBun, this.gem02, Quaternion.identity);
			Instantiate (this.sparksBun, this.gem03, Quaternion.identity);
			Instantiate (this.sparksBun, this.gem04, Quaternion.identity);
			Instantiate (this.sparksBun, this.gem05, Quaternion.identity);
		}
		if(PlayerPrefs.GetInt("CharacterID") == 4){
			Instantiate (this.gemPuff, this.gem01, Quaternion.identity);
			Instantiate (this.gemPuff, this.gem02, Quaternion.identity);
			Instantiate (this.gemPuff, this.gem03, Quaternion.identity);
			Instantiate (this.gemPuff, this.gem04, Quaternion.identity);
			Instantiate (this.gemPuff, this.gem05, Quaternion.identity);

			Instantiate (this.sparksPuff, this.gem01, Quaternion.identity);
			Instantiate (this.sparksPuff, this.gem02, Quaternion.identity);
			Instantiate (this.sparksPuff, this.gem03, Quaternion.identity);
			Instantiate (this.sparksPuff, this.gem04, Quaternion.identity);
			Instantiate (this.sparksPuff, this.gem05, Quaternion.identity);
		}
		if(PlayerPrefs.GetInt("CharacterID") == 5){
			Instantiate (this.gemDragon, this.gem01, Quaternion.identity);
			Instantiate (this.gemDragon, this.gem02, Quaternion.identity);
			Instantiate (this.gemDragon, this.gem03, Quaternion.identity);
			Instantiate (this.gemDragon, this.gem04, Quaternion.identity);
			Instantiate (this.gemDragon, this.gem05, Quaternion.identity);

			Instantiate (this.sparksDragon, this.gem01, Quaternion.identity);
			Instantiate (this.sparksDragon, this.gem02, Quaternion.identity);
			Instantiate (this.sparksDragon, this.gem03, Quaternion.identity);
			Instantiate (this.sparksDragon, this.gem04, Quaternion.identity);
			Instantiate (this.sparksDragon, this.gem05, Quaternion.identity);
		}

		if(PlayerPrefs.GetInt("CharacterID") == 6){
			Instantiate (this.gemCat, this.gem01, Quaternion.identity);
			Instantiate (this.gemCat, this.gem02, Quaternion.identity);
			Instantiate (this.gemCat, this.gem03, Quaternion.identity);
			Instantiate (this.gemCat, this.gem04, Quaternion.identity);
			Instantiate (this.gemCat, this.gem05, Quaternion.identity);

			Instantiate (this.sparksCat, this.gem01, Quaternion.identity);
			Instantiate (this.sparksCat, this.gem02, Quaternion.identity);
			Instantiate (this.sparksCat, this.gem03, Quaternion.identity);
			Instantiate (this.sparksCat, this.gem04, Quaternion.identity);
			Instantiate (this.sparksCat, this.gem05, Quaternion.identity);
		}
	}



}
