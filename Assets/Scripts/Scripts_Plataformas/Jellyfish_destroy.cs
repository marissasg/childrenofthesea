using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Jellyfish_destroy : MonoBehaviour {

	[SerializeField]
	public int life = 5;
	public Image lifeImage;

	public GameObject DefaultObject, ZombieObject, BunnyObject, PuffObject, DragonObject, CatObject, Collision_PetroleumObject;
	private Player_move playerLI;
	private Collision_petroleum colPetroleumLI;
	private Vector3 posPlayerLI, posColPetroleumLI;

	public AudioClip[] audioClip;
	private AudioSource source;

	public Image white;
	public Animator anim;

	void Start () {

		this.lifeImage.sprite=Resources.Load<Sprite> ("vida_01_");
		if (PlayerPrefs.GetInt ("CharacterID") == 1) {
			playerLI = DefaultObject.GetComponent<Player_move> ();
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 2) {
			playerLI = ZombieObject.GetComponent<Player_move> ();
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 3) {
			playerLI = BunnyObject.GetComponent<Player_move> ();
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 4) {
			playerLI = PuffObject.GetComponent<Player_move> ();
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 5) {
			playerLI = DragonObject.GetComponent<Player_move> ();
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 6) {
			playerLI = CatObject.GetComponent<Player_move> ();
		}


		colPetroleumLI = Collision_PetroleumObject.GetComponent<Collision_petroleum>();

		this.posPlayerLI = playerLI.posPlayer;
		this.posColPetroleumLI = colPetroleumLI.posColPetroleum;

	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Update () {

		this.posPlayerLI.y = playerLI.Player_Y ();
		this.posColPetroleumLI.y = colPetroleumLI.CollisionPetroleum_Y ();

		//Compara si choca con el petroleo
		if(this.posColPetroleumLI.y > this.posPlayerLI.y ){
			Invoke ("Lose",1.0f);
			this.life = 0;
			refreshImage ();
		}
			
		refreshImage ();

		if(life==0) {
			Invoke ("Lose",1.0f);
		}

	}

	void Lose(){
		StartCoroutine(Fading ());
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			playSound(0);
			this.life--;
		}
	}

	public void refreshImage(){
		if(life==4) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_02_");
		}
		if(life==3) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_03_");
		}
		if(life==2) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_04_");
		}
		if(life==1) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_05_");
		}
		if(life==0) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("clearImage");
		}
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		SceneManager.LoadScene("Lose");
		yield return new WaitUntil (()=>this.white.color.a==1);
	}


}
