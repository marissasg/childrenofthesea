﻿using System.Collections;
using UnityEngine;

public class Rotating_item : MonoBehaviour {

	public GameObject colPetroleum;
	private Collision_petroleum scriptPetroleum;
	private Vector3 posColPetroleum_;

	void Start () {
		this.scriptPetroleum = this.colPetroleum.GetComponent<Collision_petroleum> ();
		this.posColPetroleum_ = this.scriptPetroleum.posColPetroleum;
	}

	void Update () {
		transform.Rotate (new Vector3(0, Time.deltaTime*50, 0));

		this.posColPetroleum_.y = this.scriptPetroleum.CollisionPetroleum_Y ();

		if(this.posColPetroleum_.y > this.transform.position.y ){
			Destroy (this.gameObject);
		}

	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			Destroy (this.gameObject);
		}
	}
}