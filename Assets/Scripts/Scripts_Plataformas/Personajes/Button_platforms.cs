﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button_platforms : MonoBehaviour {

	public Image white;
	public Animator anim;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			Invoke ("Win", 3.0f);
		}
	}

	void Win(){
		StartCoroutine(Fading ());
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		SceneManager.LoadScene("Win");
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

}
