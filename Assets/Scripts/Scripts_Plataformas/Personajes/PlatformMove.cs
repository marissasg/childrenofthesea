﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour {


	public float runSpeed;

	Rigidbody RB;
	Animator anim;

	private bool facingRight;

	//Parámetros para el salto 
	bool grounded = false; 
	Collider[] groundCollisions;
	float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	public Transform groundCheck;
	public float jumpHeight;

	//Booleanos
	bool itCanMove = true; 

	// Use this for initialization
	void Start () {

		RB = GetComponent<Rigidbody> ();
		anim = GetComponent<Animator> ();
		facingRight = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){

		if (grounded && Input.GetButton ("Jump")) {
			print ("ESTAS OPRIMIENDO SALTO");
			grounded = false;
			anim.SetBool ("Grounded", grounded);
			//anim.SetFloat("verticalSpeed", RB.velocity.y);
			RB.AddForce (new Vector3 (0, jumpHeight, 0));
		}

		groundCollisions = Physics.OverlapSphere (groundCheck.position, groundCheckRadius, groundLayer);
		if (groundCollisions.Length > 0) {
			grounded = true;

		} else {
			grounded = false;
		}

		anim.SetBool ("isInHUB", false);
		anim.SetBool ("isInMaze",false);
		anim.SetBool ("isSeq", false);
		anim.SetBool ("ItLost", false);
		anim.SetBool ("ItWin", false);
		anim.SetBool ("Grounded", grounded);
		anim.SetFloat("verticalSpeed", RB.velocity.y);

		if (itCanMove == true) {
			float move = Input.GetAxis ("Horizontal");
			anim.SetFloat ("speed", Mathf.Abs (move));
			RB.velocity = new Vector3 (move * runSpeed, RB.velocity.y, 0);
			//RB.isKinematic = false;
			if (move > 0 && !facingRight) {
				Flip ();
			} else if (move < 0 && facingRight) {
				Flip ();
			}
		}
		//RB.isKinematic = true;
	}

	void Flip(){
		facingRight = !facingRight;
		Vector3 Scale = transform.localScale;
		Scale.z *= -1;
		transform.localScale = Scale;
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Jellyfish") {
			anim.SetBool ("isHurt", true);
		}

		if (col.gameObject.tag == "Btn_collider") {
			itCanMove = false;
			RB.velocity = new Vector3 (0, 0, 0);
			Vector3 MovButton = transform.localPosition;
			MovButton = new Vector3 (-1.94f, 42.16f, -0.26f);
			transform.localPosition = MovButton;
			Quaternion Mov = transform.localRotation;
			Mov.y = 117.0f;
			transform.localRotation = Mov;
			anim.SetBool ("ButtonWin", true);

		}
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Jellyfish") {
			anim.SetBool ("isHurt", false);
		}
	}
}
