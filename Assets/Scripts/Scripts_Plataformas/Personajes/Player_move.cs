using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_move : MonoBehaviour {

	private CharacterController controller;

	private float verticalVelocity;
	private float gravity=14.0f;
	private float jumpForce;

	public Vector3  posPlayer;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;


	private Animator anim;




	/*JUMP FORCE
	 * Normal 8.0f
	 * Conejo 12.0f
	 */

	/*
	* 1	Bag
	* 2	Zombie
	* 3	Bunn
	* 4	Puff
	* 5	Dragon
	*/


	void Start () {
		
		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		this.anim = this.GetComponent<Animator> ();

		this.posPlayer = transform.position;
		controller = GetComponent<CharacterController> ();

		if(PlayerPrefs.GetInt("CharacterID") == 3){
			this.jumpForce = 13.0f;
		}else{
			this.jumpForce = 10.0f;
		}
	}

	private void Update () {

	
		this.posPlayer = transform.position;

		if(controller.isGrounded && Input.GetAxis("Horizontal") == 0){
			this.anim.Play ("Default_Idle");
			this.anim.Play ("Zombie_Idle");
			this.anim.Play ("Bunny_Idle");
			this.anim.Play ("Puff_Idle");
			this.anim.Play ("Dragon_Idle");
			this.anim.Play ("Raccstar_Idle");
			this.anim.Play ("Picatsso_Idle");
			verticalVelocity = -gravity * Time.deltaTime;
			if(Input.GetButtonDown("Jump")){
				this.anim.Play ("Default_Jump");
				this.anim.Play ("Zombie_Jump");
				this.anim.Play ("Bunny_Jump");
				this.anim.Play ("Puff_Jump");
				this.anim.Play ("Dragon_Jump");
				this.anim.Play ("Raccstar_Jump");
				this.anim.Play ("Picatsso_Jump");
				verticalVelocity = jumpForce;
			}
		}else{
			verticalVelocity -= gravity * Time.deltaTime;
		}

		Vector3 moveVector = Vector3.zero;
		moveVector.x = Input.GetAxis ("Horizontal")*5.0f;
		moveVector.y = verticalVelocity;
		if (Input.GetAxis ("Horizontal") > 0) {
			moveVector.z = Input.GetAxis ("Horizontal");
			this.transform.LookAt(new Vector3 (90f, 0f, 0f));
			this.anim.Play ("Default_Walk");
			this.anim.Play ("Zombie_Walk");
			this.anim.Play ("Bunny_Walk");
			this.anim.Play ("Puff_Walk");
			this.anim.Play ("Dragon_Run");
			this.anim.Play ("Raccstar_Walk");
			this.anim.Play ("Picatsso_Walk");
			if(Input.GetButtonDown("Jump")){
				this.anim.Play ("Default_Jump");
				this.anim.Play ("Zombie_Jump");
				this.anim.Play ("Bunny_Jump");
				this.anim.Play ("Puff_Jump");
				this.anim.Play ("Dragon_Jump");
				this.anim.Play ("Raccstar_Jump");
				this.anim.Play ("Picatsso_Jump");
				verticalVelocity = jumpForce;
			}
		}
		if (Input.GetAxis ("Horizontal") < 0) {
			moveVector.z = Input.GetAxis("Horizontal");
			this.transform.LookAt(new Vector3 (-90f, 0f, 0f));
			this.anim.Play ("Default_Walk");
			this.anim.Play ("Zombie_Walk");
			this.anim.Play ("Bunny_Walk");
			this.anim.Play ("Puff_Walk");
			this.anim.Play ("Dragon_Run");
			this.anim.Play ("Raccstar_Walk");
			this.anim.Play ("Picatsso_Walk");
			if(Input.GetButtonDown("Jump")){
				this.anim.Play ("Default_Jump");
				this.anim.Play ("Zombie_Jump");
				this.anim.Play ("Bunny_Jump");
				this.anim.Play ("Puff_Jump");
				this.anim.Play ("Dragon_Jump");
				this.anim.Play ("Raccstar_Jump");
				this.anim.Play ("Picatsso_Jump");
				verticalVelocity = jumpForce;
			}
		}
		moveVector.z = Mathf.Clamp (moveVector.z, 0, 0.1f);
		// = Quaternion (Mathf.Clamp (this.transform.rotation.x, 0, 0.1f));
		controller.Move(moveVector* Time.deltaTime);
		//this.transform.localRotation.Set(Mathf.Clamp(this.transform.rotation.x,0,0.1f),Mathf.Clamp(this.transform.rotation.y,0,0.1f),0f,0f);
		//this.transform.Rotate(0f,0f,0f);
	}

	public float Player_Y(){
		return this.posPlayer.y;
	}

	void OnTriggerEnter(Collider col){
		
		if (col.gameObject.tag == "Jellyfish"){
			
			this.anim.Play("Default_Hurt");
			this.anim.Play ("Zombie_Hurt");
			this.anim.Play ("Bunny_Hurt");
			this.anim.Play ("Puff_Hurt");
			this.anim.Play ("Dragon_Hurt");
			this.anim.Play ("Raccstar_Hurt");
			this.anim.Play ("Picatsso_Hurt");
			//print ("JELLYFISH");
		}
		if (col.gameObject.name == "Button_Collider"){
			this.anim.Play ("Default_Push");
			this.anim.Play ("Zombie_Push");
			this.anim.Play ("Bunny_Push");
			this.anim.Play ("Puff_Push");
			this.anim.Play ("Dragon_Push");
			this.anim.Play ("Raccstar_Push");
			this.anim.Play ("Picatsso_Push");

		}



}
}



