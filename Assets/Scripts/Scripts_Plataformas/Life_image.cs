﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life_image : MonoBehaviour {

	public Image lifeImage;

	public GameObject jellyfish;
	private Jellyfish_destroy otherScriptToAccess;
	private int life_;


	public GameObject PlayerObject, Collision_PetroleumObject;
	private Player_move playerLI;
	private Collision_petroleum colPetroleumLI;
	private Vector3 posPlayerLI, posColPetroleumLI;


	void Start () {

		this.otherScriptToAccess=this.jellyfish.GetComponent<Jellyfish_destroy> ();
		this.life_ = this.otherScriptToAccess.life;

		this.lifeImage.sprite=Resources.Load<Sprite> ("vida_01_");

		playerLI = PlayerObject.GetComponent<Player_move>();
		colPetroleumLI = Collision_PetroleumObject.GetComponent<Collision_petroleum>();

		this.posPlayerLI = playerLI.posPlayer;
		this.posColPetroleumLI = colPetroleumLI.posColPetroleum;

	}

	void Update () {

		this.posPlayerLI.y = playerLI.Player_Y ();
		this.posColPetroleumLI.y = colPetroleumLI.CollisionPetroleum_Y ();

		//Compara si choca con el petroleo
		if(this.posColPetroleumLI.y > this.posPlayerLI.y ){
			print ("******* MUERTE *******");
			this.life_ = 0;
			refreshImage ();
		}

//		updateLife ();
		print ("Life LIFE IMAGE "+this.life_);
		refreshImage ();

		if(life_==0) {
			print("---------------  PERDEDOR  ---------------");
		}

	}

	public void refreshImage(){
		if(life_==4) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_02_");
		}
		if(life_==3) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_03_");
		}
		if(life_==2) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_04_");
		}
		if(life_==1) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_05_");
		}
		if(life_==0) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("clearImage");
		}
	}


}
