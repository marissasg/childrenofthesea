﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause_color : MonoBehaviour {

	public Color newColor_;
	public Image image;
	Color32 startColor= new Color32 (225, 69, 69, 100);
	Color32 endColor=new Color32(225,208,69,100);

	void Start () {
		image.GetComponent<Image> ().color = new Color32 (94, 69, 225, 100);
	}

	void Update () {
		image.color = Color.Lerp (this.startColor, this.endColor,Time.deltaTime*2);
	}
}
