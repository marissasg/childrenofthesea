﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfish_movement : MonoBehaviour {

	private float max, min, posY;
	private bool up, down;
	private Vector3 position;
	public float factorUp;

	void Start () {
		this.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		this.posY = this.position.y;
		this.max = position.y + this.factorUp;
		this.min = position.y;
		this.up = false;
		this.down = true;

	}

	void Update () {

		if(this.down==true){
			transform.Translate (new Vector3(0,posY*Time.deltaTime*-.02f, 0));

			this.position.y = transform.position.y;
			this.posY = this.position.y;

			if(this.posY<this.min){
				this.down = false;
				this.up = true;
			}
		}

		if(this.up==true){
			transform.Translate (new Vector3(0,posY*Time.deltaTime*.02f, 0));

			this.position.y = transform.position.y;
			this.posY = this.position.y;

			if(this.posY>this.max){
				this.up = false;
				this.down = true;
			}
		}

	}
}