﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_sparks : MonoBehaviour {

	public GameObject colPetroleum;
	private Collision_petroleum scriptPetroleum;
	private Vector3 posColPetroleum_;

	void Start () {
		this.scriptPetroleum = this.colPetroleum.GetComponent<Collision_petroleum> ();
		this.posColPetroleum_ = this.scriptPetroleum.posColPetroleum;
	}

	void Update () {
		this.posColPetroleum_.y = this.scriptPetroleum.CollisionPetroleum_Y ();

		if(this.posColPetroleum_.y > this.transform.position.y ){
			Destroy (this.gameObject);
		}

	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			Destroy (this.gameObject);
		}
	}

}
