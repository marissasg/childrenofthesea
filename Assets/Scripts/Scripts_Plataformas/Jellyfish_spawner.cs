﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfish_spawner : MonoBehaviour {

	public GameObject jellyfish;
	private Vector3 enemy01, enemy02, enemy03, enemy04;

	void Start () {

		this.enemy01 = new Vector3 (-3.04f, 12.32f, 0f);
		this.enemy02 = new Vector3 (-5.83f, 24.39f,0f);
		this.enemy03 = new Vector3 (2.92f, 36.76f, 0f);
		this.enemy04 = new Vector3 (2.92f, 31.37f,0f);

		Instantiate (this.jellyfish, this.enemy01, Quaternion.identity);
		Instantiate (this.jellyfish, this.enemy02, Quaternion.identity);
		Instantiate (this.jellyfish, this.enemy03, Quaternion.identity);
		Instantiate (this.jellyfish, this.enemy04, Quaternion.identity);
	
	}
		
}
