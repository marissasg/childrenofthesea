﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_follow : MonoBehaviour {

	[SerializeField]
	//private float xMax, yMax, xMin, yMin;
	private float yMax, yMin;

	private Transform target;

	void Start () {
		if (PlayerPrefs.GetInt ("CharacterID") == 1) {
			target = GameObject.Find ("newraccstar_animations").transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 2) {
			target = GameObject.Find ("Squishy_zombie").transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 3) {
			target = GameObject.Find ("squishy_bunny").transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 4) {
			target = GameObject.Find ("squishy_puff").transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 5) {
			target = GameObject.Find ("squishy_dragon").transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 6) {
			target = GameObject.Find ("newpicatsso_animations").transform;
		}
	}

	void LateUpdate () {
		//transform.position = new Vector3 (Mathf.Clamp(target.position.x, xMin, xMax), Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);
		transform.position = new Vector3 (transform.position.x, Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);
	}
}
