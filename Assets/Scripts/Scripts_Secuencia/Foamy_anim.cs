﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foamy_anim : MonoBehaviour {
	private Animator anim;

	public AudioClip[] audioClip;
	private AudioSource source;

	void Start () {
		this.anim=this.GetComponent<Animator>();
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "InitialTrash"||col.gameObject.tag == "NextToDestroy"){
			playSound(0);
			this.anim.Play ("Foamy_Hurt");
			StartCoroutine ("wait");
			//print ("PLAY HURT");
		}
		if (col.gameObject.tag == "Gem_Bone" || col.gameObject.tag == "Gem_Carrot"||col.gameObject.tag == "Gem_Coral"||col.gameObject.tag == "Gem_Crown"||col.gameObject.tag == "Gem_Fire"||col.gameObject.tag == "Gem_Fish"){
			this.anim.Play ("Foamy_Celebration");
			StartCoroutine ("wait");
			//print ("PLAY CELEBRATION");
		}
	}

	IEnumerator wait(){
		yield return new WaitForSeconds (1);
		this.anim.Play ("Foamy_Idle");
		print ("idle");
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}


}
