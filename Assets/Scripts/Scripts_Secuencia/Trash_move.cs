﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash_move : MonoBehaviour {

	private float speed;
	private Vector3 pos;

	void Start () {
		this.speed = -0.06f;
		this.pos = transform.position;
	}

	void Update () {

		//if(gameObject.tag=="InitialTrash"){
		//	this.gameObject.tag = "Trash";
		//}

		this.pos.z += this.speed;
		transform.position = this.pos;

	}
}
