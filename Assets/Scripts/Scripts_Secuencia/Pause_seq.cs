﻿using System.Collections;
using UnityEngine;

public class Pause_seq : MonoBehaviour {

	private Canvas pauseCanvas;

	public AudioClip[] audioClip;
	private AudioSource source;

	void Start () {
		this.pauseCanvas = GetComponent<Canvas> ();
		this.pauseCanvas.enabled = false;
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Update () {
		if(Input.GetButton("Start")){
			if(Time.timeScale==1){
				playSound(0);
				this.pauseCanvas.enabled = true;
				Time.timeScale = 0;
			}else{
				playSound(1);
				this.pauseCanvas.enabled = false;
				Time.timeScale = 1;
			}
		}

	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}
