﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTag : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "InitialTrash"){
			col.gameObject.tag = "NextToDestroy";
		}
	}

}
