﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Hurt_trash : MonoBehaviour {

	private int life = 5;
	public Image lifeImage;

	public Image white;
	public Animator anim;

	void Start () {
		this.lifeImage.sprite=Resources.Load<Sprite> ("vida_01_");
	}

	void Update () {
		refreshImage ();

		if(life==0) {
			Invoke ("Lose",1.0f);
		}
	}
		
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "InitialTrash" ){
			this.life--;
		}
		if (col.gameObject.tag == "NextToDestroy" ){
			this.life--;
		}
	}

	public void refreshImage(){
		if(life==4) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_02_");
		}
		if(life==3) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_03_");
		}
		if(life==2) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_04_");
		}
		if(life==1) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_05_");
		}
		if(life==0) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("clearImage");
		}
	}

	public void Lose(){
		StartCoroutine(Fading ());

	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		SceneManager.LoadScene("Lose");
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

}
