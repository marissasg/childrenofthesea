using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprites_Sequence : MonoBehaviour {

	public AudioClip[] audioClip;
	private AudioSource source;

	public bool newSequence;
	public bool refreshImage;

	public List<int> gameSequence = new List<int> ();
	public List<int> inputSequence = new List<int> ();
	public List<int> validationSequence = new List<int> ();

	//VALIDATION
	private Vector3 val01 = new Vector3(-0.8054f, 2.75f, -9.18f);
	private Vector3 val02 = new Vector3(-0.3206f, 2.75f, -9.18f);
	private Vector3 val03 = new Vector3(0.1808f, 2.75f, -9.18f);
	private Vector3 val04 = new Vector3(0.64f, 2.75f, -9.18f);
	private Vector3 val05 = new Vector3(1.14f, 2.75f, -9.18f);
	GameObject[] ValidationSprites;
	private int actualCase = 1;

	public GameObject sprRight, sprWrong;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

	//RANDOM TRASH
	private int randomID;
	public GameObject can, bottle, tire, barrel, gemCoral, gemBone, gemCarrot, gemCrown, gemFire, gemFish, laser;
	private Vector3 initialPos, laserInitialPos;

	private float time, timeAux;
	private int aux=0;
	private int limit;

	bool upBool=false;
	bool downBool=false;
	bool leftBool=false;
	bool rightBool=false;


	//0 es A	4 es arriba
	//1 es B	5 es abajo
	//2 es X	6 es izquierda
	//3 es Y	7 es derecha
	/* 0		lata
	 * 1		botella
	 * 2 		llanta
	 * 3		barril
	 * 4		gema 
	 */

	void Start () {
		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		this.newSequence = false;
		this.refreshImage = false;

		if(PlayerPrefs.GetInt("CharacterID") == 4){
			this.limit = 3;
			for(int i=0 ; i < 5 ; i++){
				if(i<3){
					gameSequence.Add (Random.Range (0,4));
				}
				if(i==3){
					gameSequence.Add (8);
				}
				if(i==4){
					gameSequence.Add (8);
				}

			}
		}else{
			this.limit = 5;
			for(int i=0 ; i < 5 ; i++){
				gameSequence.Add (Random.Range (0,4));
			}
		}

		//RANDOM TRASH
		this.initialPos = new Vector3 (1.9f, 0.3f, 14f);
		createTrash();

		//LASER
		this.laserInitialPos=new Vector3(0.06f, 0.458f, -5.802f);

	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Update () {

		timeConfig ();


		#region IF KEY DOWN 0 AL 7

		if(Input.GetButtonDown("Jump")){
			//print ("A");
			inputSequence.Add (0);
			compare();
			this.refreshImage=false;


		}
		if(Input.GetButtonDown("B")){
			//print ("B");

			inputSequence.Add (1);
			compare();
			this.refreshImage=false;

		}
		if(Input.GetButtonDown("X")){
			//print ("X");

			inputSequence.Add (2);
			compare();
			this.refreshImage=false;

		}
		if(Input.GetButtonDown("Y")){
			//print ("Y");

			inputSequence.Add (3);
			compare();
			this.refreshImage=false;

		}

		/*
		if(Input.GetAxisRaw("Vertical D-Pad") < 0){
			
			if(this.upBool == false){
				print ("Up");

				inputSequence.Add (4);
				compare();
				this.refreshImage=false;
				this.upBool = true;

			 }
			     
			if( Input.GetAxisRaw("Vertical D-Pad") == 0){
				this.upBool = false;

			}

		}

		if(Input.GetAxis("Vertical D-Pad") > 0){

			if(this.downBool == false){
				print ("Down");

				inputSequence.Add (5);
				compare();
				this.refreshImage=false;
				this.downBool = true;

			}

			if( Input.GetAxisRaw("Vertical D-Pad") == 0){
				this.downBool = false;

			}

		}

		if(Input.GetAxis("Horizontal D-Pad") < 0){

			if(this.leftBool == false){
				print ("Left");
				inputSequence.Add (6);
				compare();
				this.refreshImage=false;
				this.leftBool = true;

			}

			if( Input.GetAxisRaw("Horizontal D-Pad") == 0){
				this.leftBool = false;

			}

		}

		if(Input.GetAxis("Horizontal D-Pad") > 0){

			if(this.rightBool == false){
				print ("Right");
				inputSequence.Add (7);
				compare();
				this.refreshImage=false;
				this.rightBool = true;

			}

			if( Input.GetAxisRaw("Horizontal D-Pad") == 0){
				this.rightBool = false;

			}

		}
		*/

		#endregion

		#region CREAR NUEVA SECUENCIA
		if(newSequence==true){

			inputSequence.Clear();
			gameSequence.Clear();

			if(PlayerPrefs.GetInt("CharacterID") == 4){
				for(int i=0 ; i < 5 ; i++){
					if(i<3){
						gameSequence.Add (Random.Range (0,4));
					}
					if(i==3){
						gameSequence.Add (8);
					}
					if(i==4){
						gameSequence.Add (8);
					}

				}
			}else{
				for(int i=0 ; i < 5 ; i++){
					gameSequence.Add (Random.Range (0,4));
				}
			}

//			print ("NUEVA secuencia real ");
			for(int i=0 ; i < PlayerPrefs.GetInt("CharacterID") ; i++){ //ORIGINAL ES characterID_ 
//				print (gameSequence[i]);
			}

			this.refreshImage=true;
			this.newSequence=false;


		}

		#endregion

		#region COMPROBAR SENCUENCIA REAL CON INPUT

		if(inputSequence.Count==this.limit){
			aux=0;
			DestroyValidation();
			bool gameOver=false;

			if(PlayerPrefs.GetInt("CharacterID") == 4){ //PUFF
				for(int i=0 ; i < 3 ; i++){
					if(gameOver==false){
						if(inputSequence[i]!=gameSequence[i]){
							gameOver=true;
							//print("PERDEDOR"); 
						}
					}
				}
				if(gameOver==false){
					//print("GANADOR");
					playSound(2);
					Instantiate (this.laser, this.laserInitialPos, laser.transform.rotation);
				}

			}else{ 
				for(int i=0 ; i < gameSequence.Count ; i++){
					if(gameOver==false){
						if(inputSequence[i]!=gameSequence[i]){
							gameOver=true;
							//print("PERDEDOR");
						}
					}
				}
				if(gameOver==false){
					//print("GANADOR");
					playSound(2);
					Instantiate (this.laser, this.laserInitialPos, laser.transform.rotation);
				}

			}

			newSequence=true;

		}
		#endregion

	}

	public void createTrash(){
		
			randomID = Random.Range (0, 5);

			if(this.randomID == 0){
			Instantiate (this.can, this.initialPos, can.transform.rotation);
				//print ("0   Lata");
			}
			if(this.randomID == 1){
			Instantiate (this.bottle, this.initialPos, bottle.transform.rotation);
				//print ("1   Botella");
			}
			if(this.randomID == 2){
			Instantiate (this.tire, this.initialPos, tire.transform.rotation);
				//print ("2   Llanta");
			}
			if(this.randomID == 3){
			Instantiate (this.barrel, this.initialPos, barrel.transform.rotation);
				//print ("3   Barril");
			}
			if(this.randomID == 4){
			if(PlayerPrefs.GetInt("CharacterID") == 1){
				Instantiate (this.gemCoral, this.initialPos, gemCoral.transform.rotation);
				}
			if(PlayerPrefs.GetInt("CharacterID") == 2){
				Instantiate (this.gemBone, this.initialPos, gemBone.transform.rotation);
				}
			if(PlayerPrefs.GetInt("CharacterID") == 3){
				Instantiate (this.gemCarrot, this.initialPos, gemCarrot.transform.rotation);
				}
			if(PlayerPrefs.GetInt("CharacterID") == 4){
				Instantiate (this.gemCrown, this.initialPos, gemCrown.transform.rotation);
				}
			if(PlayerPrefs.GetInt("CharacterID") == 5){
				Instantiate (this.gemFire, this.initialPos, gemFire.transform.rotation);
				}

			if(PlayerPrefs.GetInt("CharacterID") == 6){
				Instantiate (this.gemFish, this.initialPos, gemFish.transform.rotation);
			}

			}

		}

	public int valueInput(){
		return this.inputSequence[this.inputSequence.Count];
	}

	public void compare(){
		if(this.gameSequence[aux]==this.inputSequence[aux]){
			//print ("BIEN");
			playSound(0);
			Instantiate (this.sprRight, assignPos (), Quaternion.identity);
		}else
		{
			playSound(1);
			Instantiate (this.sprWrong, assignPos (), Quaternion.identity);
		}
		aux++;
	}

	public Vector3 assignPos (){
//		print (aux);
		if(this.aux==0){
			return this.val01;
		}
		if(this.aux==1){
			return this.val02;
		}
		if(this.aux==2){
			return this.val03;
		}
		if(this.aux==3){
			return this.val04;
		}
		if(this.aux==4){
			return this.val05;
		}
		return this.val01;
	}

	public void DestroyValidation(){
		this.ValidationSprites = GameObject.FindGameObjectsWithTag ("Validation_sprites");
		foreach(GameObject v in ValidationSprites){
			Destroy (v.gameObject);
		}
	}

	public void timeConfig(){
		this.time += Time.deltaTime;
		this.timeAux += Time.deltaTime;

		if(this.timeAux<6){
			actualCase = 1;
		}
		if(this.timeAux>=6  && this.timeAux<15){
			actualCase = 2;
		}
        if(this.timeAux>=15){
			actualCase = 3;
		}

        switch (actualCase){
			
            case 1:
                if(this.time>3.5){
				print ("TIME 4");
			        this.time = 0;
			        createTrash ();
		        }
                break;

            case 2:
                if(this.time>2){
				print ("TIME 2");
			        this.time = 0;
			        createTrash ();
		        }
                break;

            case 3:
                if(this.time>1){
				print ("TIME 1");
			        this.time = 0;
			        createTrash ();
		        }
                break;
		}

	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}
