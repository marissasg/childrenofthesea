﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeImage : MonoBehaviour {

	public int index;
	public GameObject spriteMain;
	public Sprite button_A, button_B, button_X, button_Y, button_Up, button_Down, button_Left, button_Right, clearImage;

	private Sprites_Sequence scriptToAccess;
	private List<int> gameSequenceCI = new List<int> ();
	private bool refreshImageCI, newSequenceCI;

	//VALIDATION
	/*
	public Sprite clear, good, bad;
	//public int indexV;
	private int actualInputSizeCI, auxSize;
	private List<int> inputSequenceCI = new List<int> ();
	*/

	void Start () {

		scriptToAccess= spriteMain.GetComponent<Sprites_Sequence> ();

		for(int i=0;i<scriptToAccess.gameSequence.Count;i++){
			this.gameSequenceCI.Add (scriptToAccess.gameSequence [i]); 
		}

		updateImage ();

		this.refreshImageCI = scriptToAccess.refreshImage;
		this.newSequenceCI = scriptToAccess.newSequence;

		/*this.actualInputSizeCI = scriptToAccess.actualInputSize;
		this.auxSize = this.actualInputSizeCI;*/

	}


	void Update () {
		
		this.refreshImageCI = scriptToAccess.refreshImage;


		/*this.actualInputSizeCI = scriptToAccess.actualInputSize;

		if(this.actualInputSizeCI!=this.auxSize){
			updateValidation ();
			this.auxSize = this.actualInputSizeCI;
		}
*/


		if(this.refreshImageCI==true){
//			print ("refresh image CI es TRUE");
			updateSequence ();
			this.refreshImageCI = false;
		}
			
	}


	void updateSequence(){

		this.gameSequenceCI.Clear ();

		for(int i=0;i<scriptToAccess.gameSequence.Count;i++){
			this.gameSequenceCI.Add (scriptToAccess.gameSequence [i]); 
		}
		updateImage ();
	}

	void updateImage() {

		//print ("UPDATE IMAGE");
		if(gameSequenceCI[index]==0){
			//print ("image A");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_A;
		}
		if(gameSequenceCI[index]==1){
			//print ("image B");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_B;
		}
		if(gameSequenceCI[index]==2){
			//print ("image X");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_X;
		}
		if(gameSequenceCI[index]==3){
			//print ("image Y");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_Y;
		}
		if(gameSequenceCI[index]==4){
			//print ("image UP");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_Up;
		}
		if(gameSequenceCI[index]==5){
			//print ("image DOWN");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_Down;
		}
		if(gameSequenceCI[index]==6){
			//print ("image LEFT");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_Left;
		}
		if(gameSequenceCI[index]==7){
			//print ("image RIGHT");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = button_Right;
		}
		if(gameSequenceCI[index]==8){
			//print ("image CLEAR IMAGE");
			//print (gameSequenceCI[index]);
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = clearImage;
		}
	
	}

	/*
	void updateValidation(){
		//revisar si mi ultimo input es igual a el correspondiente game sequence
		print("update validation");
		print(gameSequenceCI[actualInputSizeCI]);
		this.inputSequenceCI = scriptToAccess.inputSequence;

		if(this.gameSequenceCI[actualInputSizeCI]==this.inputSequenceCI[actualInputSizeCI]){
			print("IGUALES");
		}


	}*/

}
	