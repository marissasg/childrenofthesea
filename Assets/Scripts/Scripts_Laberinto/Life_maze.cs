﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Life_maze : MonoBehaviour {

	private int life = 5;

	[SerializeField]
	public Image lifeImage;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

	public AudioClip[] audioClip;
	private AudioSource source;

	public Image white;
	public Animator anim;


	void Start () {
		this.lifeImage.sprite=Resources.Load<Sprite> ("vida_01_");

		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Update () {
		refreshImage ();

		if(life==0) {
			Invoke ("Lose",1.0f);
		}
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		yield return new WaitUntil (()=>this.white.color.a==1);
		SceneManager.LoadScene("Lose");

	}

	void OnTriggerEnter(Collider col){
		/*if (col.gameObject.tag == "Urchin"){
			this.life--;
			playSound(0);
			print ("COLLIDE");
		}*/
		if (col.gameObject.tag == "Puddle"){
			if (PlayerPrefs.GetInt("CharacterID") != 2){
				this.life--;
				playSound(0);
				//print ("COLLIDE");
			}
		}
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Urchin"){
			this.life--;
			playSound(0);
			//print ("COLLIDE");
		}
	}

	public void refreshImage(){
		if(life==4) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_02_");
		}
		if(life==3) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_03_");
		}
		if(life==2) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_04_");
		}
		if(life==1) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("vida_05_");
		}
		if(life==0) {
			this.lifeImage.sprite=Resources.Load<Sprite> ("clearImage");
		}
	}

	public void Lose(){
		
		StartCoroutine(Fading ());
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}