﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LabyrinthPlayerMovement : MonoBehaviour {

	//Velocidad 
	private float PlayerSpeed;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

	//Variable para animaciones 
	private Animator anim;

	public Image CatDetect;
	private bool catIsWarning = false; 

	public Image white;
	public Animator anim_;

	void Start () {
		this.anim = this.GetComponent<Animator> ();
		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		if(PlayerPrefs.GetInt("CharacterID")==5) {
			this.PlayerSpeed = 5.0f;
		}else {
			this.PlayerSpeed = 3.5f;
		}
		anim.SetBool ("isInMaze",true);


	}
	
	// Update is called once per frame
	void Update () {
		float move = (Input.GetAxis ("Horizontal")) + (Input.GetAxis("Vertical"));
		anim.SetFloat ("speed", Mathf.Abs (move));
		if (Input.GetAxis ("Horizontal") > 0) { //Derecha
			transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Horizontal") < 0) { //Izquierda
			transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (-90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Vertical") > 0) { //Arriba
			transform.Translate (Vector3.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (0f, 0f, 90f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Vertical") < 0) { //Abajo
			transform.Translate (Vector3.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (0f, 0f, -90f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
			

	}

	void OnTriggerEnter(Collider col){ //OnTriggerEnter funciona para colliders con isTrigger = TRUE y SIN rigidbody 
		if (col.gameObject.tag == "Puddle"){
			if (PlayerPrefs.GetInt("CharacterID")!=2){
				anim.SetBool ("isHurt", true);
			}
		}
		if (col.gameObject.tag == "GoalCollider") {
			Invoke ("Win",1.0f);
		}
		if (col.gameObject.tag == "Detect" && PlayerPrefs.GetInt ("CharacterID") == 6) {
			catIsWarning = true;
			this.CatDetect.sprite = Resources.Load<Sprite> ("cat_warning");
		}
	}

	void Win(){
		StartCoroutine(Fading ());
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Detect" && PlayerPrefs.GetInt ("CharacterID") == 6) {
			catIsWarning = false; 
			this.CatDetect.sprite = Resources.Load<Sprite> ("clearImage");
		}
		if (col.gameObject.tag == "Puddle") {
			anim.SetBool ("isHurt", false);
		}
	}

	void OnCollisionEnter(Collision col){ //CollisionEnter funciona para colliders con isTrigger = FALSE y CON rigidbody
		if (col.gameObject.tag == "Urchin") {
				anim.SetBool ("isHurt", true);
		}
	}

	void OnCollisionExit(Collision col){
		if (col.gameObject.tag == "Urchin") {
			anim.SetBool ("isHurt", false);
		}
	}


	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		SceneManager.LoadScene("Win");
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

}
