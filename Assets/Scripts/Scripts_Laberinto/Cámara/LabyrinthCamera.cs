﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabyrinthCamera : MonoBehaviour {
	[SerializeField]
	private float distance = 2.0f; 


	//GameObjects que contendrán a cada personaje
	public GameObject Bag; 
	public GameObject Zombie; 
	public GameObject Bunny; 
	public GameObject Puff; 
	public GameObject Dragon;
	public GameObject Cat;

	//Hacia donde va a ver la cámara
	private Transform lookAt;

	//Movimiento de la cámara
	public Transform camTransform; 

	private Camera cam;


	private float currentX = 0.0f;
	private float currentY = 85.0f;
	private float sensivityX = 4.0f;
	private float sensivityY = 1.0f;


	private void Start(){

		camTransform = transform;
		cam = Camera.main;

		//Condiciones para que la cámara mire dependiendo el personaje 
		if (PlayerPrefs.GetInt ("CharacterID") == 1) {
			lookAt = Bag.transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 2) {
			lookAt = Zombie.transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 3) {
			lookAt = Bunny.transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 4) {
			lookAt = Puff.transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 5) {
			lookAt = Dragon.transform;
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 6) {
			lookAt = Cat.transform;
		}
	}

	private void Update(){
		//currentX += Input.GetAxis ("Mouse X");
		//currentY += Input.GetAxis ("Mouse Y"); 
		//Limitamos qué tanto se puede mover ĺa vista de la cámara 
		//currentY = Mathf.Clamp(currentY,Y_ANGLE_MIN,Y_ANGLE_MAX);

	}

	private void LateUpdate(){
		Vector3 dir = new Vector3(0,0,-distance); 
		Quaternion rotation = Quaternion.Euler(currentY,currentX,0); 
		camTransform.position = lookAt.position + rotation * dir;
		camTransform.LookAt (lookAt.position);
		//Player.transform.LookAt (lookAt.position);
	}

}
