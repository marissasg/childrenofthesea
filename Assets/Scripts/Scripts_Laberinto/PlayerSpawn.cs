﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

	#region Variables para spawnear
	//public GameObject Player;
	public GameObject GemCoral, GemBone, GemCarrot, GemCrown, GemFire, GemFish; 
	public GameObject Goal;
	private int randomNum; 
	#endregion

	//Posicion de gemas
	private Vector3 gemPos01 = new Vector3(-6.504f, 5.95f, 1.787f);
	private Vector3 gemPos02 = new Vector3(-5.52f, 5.95f, -3.86f);
	private Vector3 gemPos03 = new Vector3(-0.67f, 5.95f, -1.654f);
	private Vector3 gemPos04 = new Vector3(5.285f, 5.95f, -3.52f);
	private Vector3 gemPos05 = new Vector3(5.73f, 5.95f, 0.42f);
	private Vector3 gemPos06 = new Vector3(2.1f, 5.95f, -0.84f);

	//Lista de Spawneo de la meta
	private List<Vector3> PosPlayer = new List<Vector3>(); 

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

	void Start () {

		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		//LabCam = CameraScript.GetComponent<LabyrinthCamera> ();

		//Inicialización de los contenedores 
		//GameObject PlayerCont = new GameObject();
		//GameObject PuddleCont = new GameObject ();
		GameObject GemCont = new GameObject ();
		//GameObject GoalCont = new GameObject ();

		#region Posiciones 
		//Posiciones del Jugador
		PosPlayer.Add (new Vector3 (7.373f, 5.8f, -3.95f));
		PosPlayer.Add (new Vector3 (7.73f, 5.8f, 3.46f));
		PosPlayer.Add (new Vector3 (-7.56f, 5.8f, 3.46f));
		PosPlayer.Add (new Vector3 (-7.717f, 5.8f, -1.82f));
		PosPlayer.Add (new Vector3 (-5.45f, 5.8f, -0.62f));
		PosPlayer.Add (new Vector3 (-2.67f, 5.8f, 1.54f));

		//Posiciones de las gemas 
		#region Gems

		switch (PlayerPrefs.GetInt("CharacterID")) {

		case 1: // coral
			//GemCont = Instantiate (this.GemCoral, GameObject.Find ("PosGem(Clone)").transform.position, Quaternion.identity) as GameObject;
			//Destroy(GameObject.Find ("PosGem(Clone)"));
			/*GemCont = Instantiate (this.GemCoral, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCoral, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCoral, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCoral, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCoral, this.gemPos06, Quaternion.identity) as GameObject;*/
			break;
		case 2: // bone
			GemCont = Instantiate (this.GemBone, this.gemPos01, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemBone, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemBone, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemBone, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemBone, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemBone, this.gemPos06, Quaternion.identity) as GameObject;
			break; 
		case 3: // carrot
			GemCont = Instantiate (this.GemCarrot, this.gemPos01, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCarrot, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCarrot, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCarrot, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCarrot, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCarrot, this.gemPos06, Quaternion.identity) as GameObject;
			break; 
		case 4: // crown 
			GemCont = Instantiate (this.GemCrown, this.gemPos01, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCrown, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCrown, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCrown, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCrown, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemCrown, this.gemPos06, Quaternion.identity) as GameObject;
			break;
		case 5: // fire
			GemCont = Instantiate (this.GemFire, this.gemPos01, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFire, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFire, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFire, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFire, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFire, this.gemPos06, Quaternion.identity) as GameObject;
			break; 

		case 6: // fish
			GemCont = Instantiate (this.GemFish, this.gemPos01, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFish, this.gemPos02, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFish, this.gemPos03, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFish, this.gemPos04, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFish, this.gemPos05, Quaternion.identity) as GameObject;
			GemCont = Instantiate (this.GemFish, this.gemPos06, Quaternion.identity) as GameObject;
			break; 
		}
			
		#endregion

		#endregion

		/*#region Spawneo Aleatorio
		randomNum = Random.Range (0, 5); 
		switch (randomNum) {

		case 0:
			Goal.transform.position = PosPlayer[1];
			//GoalCont = Instantiate (Goal, PosPlayer[1], Quaternion.identity) as GameObject;
			break;
		case 1: 
			Goal.transform.position = PosPlayer[2];
			//GoalCont = Instantiate (Goal, PosPlayer[2], Quaternion.identity) as GameObject;
			break; 
		case 2: 
			Goal.transform.position = PosPlayer[3];
			//GoalCont = Instantiate (Goal, PosPlayer[3], Quaternion.identity) as GameObject;
			break; 
		case 3: 
			Goal.transform.position = PosPlayer[4];
			//GoalCont = Instantiate (Goal, PosPlayer[4], Quaternion.identity) as GameObject;
			break;
		case 4: 
			Goal.transform.position = PosPlayer[5];
			//GoalCont = Instantiate (Goal, PosPlayer[5], Quaternion.identity) as GameObject;
			break; 
		case 5: 
			Goal.transform.position = PosPlayer[0];
			//GoalCont = Instantiate (Goal, PosPlayer[0], Quaternion.identity) as GameObject;
			break;

		}
		#endregion*/

	}

}
