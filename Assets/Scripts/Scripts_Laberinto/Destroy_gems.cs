﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_gems : MonoBehaviour {

	public AudioClip[] audioClip;
	private AudioSource source;

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			//playSound(0);
			//Destroy (this.gameObject, this.audioClip[0].length);
			Invoke ("Destroy",0f);
		}
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

	void Destroy() {
		Destroy (this.gameObject);
	}

}
