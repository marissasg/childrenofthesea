﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Text_shop : MonoBehaviour {

	public Text coral = null;
	public Text bone = null;
	public Text carrot = null;
	public Text crown = null;
	public Text fire = null;
	public Text fish = null;

	void Start () {
		this.coral.text = PlayerPrefs.GetInt ("CoralGem").ToString();
		this.bone.text = PlayerPrefs.GetInt ("BoneGem").ToString();
		this.carrot.text = PlayerPrefs.GetInt ("CarrotGem").ToString();
		this.crown.text = PlayerPrefs.GetInt ("CrownGem").ToString();
		this.fire.text = PlayerPrefs.GetInt ("FireGem").ToString();
		//this.fish.text = PlayerPrefs.GetInt ("FishGem").ToString();
	}
	

}
