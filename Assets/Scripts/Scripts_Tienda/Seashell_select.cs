using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Seashell_select : MonoBehaviour {

	private int index = 1;
	private int totalLeves = 6;
	private int deleteVariable = 2; // ESTA VARIABLE SE PUEDE QUITAR DEPUÉS, SOLO SE USÓ PARA HACER PRUEBAS DE CAMBIAR LOS LETREROS ATTE GABY

	private Vector3 pos01 = new Vector3 (1.64f, 3.0f, -15.20f); //RACCSTAR
	private Vector3 pos02 = new Vector3 (1.163f, 3.0f, -15.13f); //ZOMBIE
	private Vector3 pos03 = new Vector3 (0.759f, 3.0f, -14.92f); //MAD B
	private Vector3 pos04 = new Vector3 (0.3279f, 3.0f, -14.65f); //PUFF
	private Vector3 pos05 = new Vector3 (-0.021f, 3.0f, -14.30f); //DRAGON
	private Vector3 pos06 = new Vector3 (-0.423f, 3.0f, -13.94f);//PICATSSO

	//Audio
	public AudioClip[] audioClip;
	private AudioSource source; 

	public GameObject bag, zombie, bunny, puff, dragon, cat, l_zombie, l_bunny, l_puff, l_dragon, l_cat; 
	private Vector3 actualPos = new Vector3 (0f, 3.5f, 0f);
	private Vector3 hidePos = new Vector3 (0f, 0.22f, 0f);
	private bool isUnlockZombie; 

	//Booleanos para impedir que se exceda el botón del control
	//private bool isPressed = false; 

	//Booleanos para desbloquear a los personajes
	private bool ZombieIsUnlocked = false;
	private bool BunnyIsUnlocked = false;
	private bool PuffIsUnlocked = false;
	private bool DragonIsUnlocked = false;
	private bool CatIsUnlocked = false;


	//Variables para el número de gemas 
	private int BerryGem, BoneGem, CarrotGem, CrownGem, FireGem; 

	//public Sprite board;
	//GemCoral, GemBone, GemCarrot, GemCrown, GemFire,

	public Image white;
	public Animator anim;

	void Start () {
		//Pasamos los valores de cada gema que ha recolectado el jugador desde el archivo de guardado 
		BerryGem = PlayerPrefs.GetInt("CoralGem");
		BoneGem = PlayerPrefs.GetInt("BoneGem");
		CarrotGem = PlayerPrefs.GetInt("CarrotGem");
		CrownGem = PlayerPrefs.GetInt("CrownGem");
		FireGem = PlayerPrefs.GetInt("FireGem");
		//this.board=Resources.Load<Sprite> ("board_bag");
		changePosition (1);
	}

	void Awake(){
		source = GetComponent<AudioSource> ();
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

	IEnumerator Wait(){
		yield return new WaitForSeconds (0.9f);
		SceneManager.LoadScene("HUB");
	}

	void Update () {
		//Si quieres que siga funcionando con botón X
		//if (Input.GetButtonDown("X")) {
		//Y este es para usar la flecha IZQUIERDA del teclado
		if (Input.GetButtonDown("Horizontal D-Pad") && Input.GetAxis("Horizontal D-Pad") < 0) {
			if (this.index<this.totalLeves) {//<>
				this.index++;
				changePosition (index);
				print (this.index);
			}
		}
		//Si quieres que siga funcionando con botón B
		//if (Input.GetButtonDown("B")) {
		//Y este es para usar la flecha DERECHA del teclado 
		if (Input.GetButtonDown("Horizontal D-Pad") &&  Input.GetAxis("Horizontal D-Pad") > 0) {
			if (this.index > 1) {//<>
				this.index--;
				changePosition (this.index);
				print (this.index);
			}
		}
			
		if(Input.GetButton("Jump")){
			if (index == 1) {
				PlayerPrefs.SetInt ("CharacterID", 1);
				PlayerPrefs.Save ();
				StartCoroutine(Fading ());
				StartCoroutine(Wait ());
				//SceneManager.LoadScene ("HUB");
			}
			if (index == 2) { // Zombie 
				if (BerryGem >= 5 && PlayerPrefs.GetInt("WalkingZombie") == 0) {
					PlayerPrefs.SetInt ("CharacterID", 2);
					PlayerPrefs.SetInt ("CoralGem", BerryGem - 5);
					PlayerPrefs.SetInt ("WalkingZombie", 1);
					PlayerPrefs.Save ();
					StartCoroutine(Fading ());
					StartCoroutine(Wait ());
					//SceneManager.LoadScene ("HUB");
				} else {

				}
			}
			if (index == 3) { // Bunny 
				if (BerryGem >= 10 && BoneGem >= 5 && PlayerPrefs.GetInt("MadBunny") == 0) {
					PlayerPrefs.SetInt ("CharacterID", 3);
					PlayerPrefs.SetInt ("CoralGem", BerryGem - 10);
					PlayerPrefs.SetInt ("BoneGem", BoneGem - 5);
					PlayerPrefs.SetInt ("MadBunny", 1);
					PlayerPrefs.Save ();
					StartCoroutine(Fading ());
					StartCoroutine(Wait ());
					//SceneManager.LoadScene ("HUB");
				} else {

				}
			}
			if (index == 4) { // Puff 
				if (BerryGem >= 10 && BoneGem >= 5 && CarrotGem >= 5 && PlayerPrefs.GetInt("PrincessPuff") == 0) {
					PlayerPrefs.SetInt ("CharacterID", 4);
					PlayerPrefs.SetInt ("CoralGem", BerryGem - 10);
					PlayerPrefs.SetInt ("BoneGem", BoneGem - 5);
					PlayerPrefs.SetInt ("CarrotGem", CarrotGem - 5);
					PlayerPrefs.SetInt ("PrincessPuff", 1);
					PlayerPrefs.Save ();
					StartCoroutine(Fading ());
					StartCoroutine(Wait ());
					//SceneManager.LoadScene ("HUB");
				} else {

				}
			}
			if (index == 5) { //Dragon 
				if (BerryGem >= 15 && BoneGem >= 10 && CarrotGem >= 5 && CrownGem >= 5 && PlayerPrefs.GetInt("Dragon") == 0) {
					PlayerPrefs.SetInt ("CharacterID", 5);
					PlayerPrefs.SetInt ("CoralGem", BerryGem - 15);
					PlayerPrefs.SetInt ("BoneGem", BoneGem - 10);
					PlayerPrefs.SetInt ("CarrotGem", CarrotGem - 5);
					PlayerPrefs.SetInt ("CrownGem", CrownGem - 5);
					PlayerPrefs.SetInt ("Dragon", 1);
					PlayerPrefs.Save ();
					StartCoroutine(Fading ());
					StartCoroutine(Wait ());
					//SceneManager.LoadScene ("HUB");
				}
			}
			if (index == 6) { //Cat
				if (BerryGem >= 20 && BoneGem >= 15 && CarrotGem >= 10 && CrownGem >= 10 && FireGem >= 10 && PlayerPrefs.GetInt("Picatsso") == 0) {
					PlayerPrefs.SetInt ("CharacterID", 5);
					PlayerPrefs.SetInt ("CoralGem", BerryGem - 20);
					PlayerPrefs.SetInt ("BoneGem", BoneGem - 15);
					PlayerPrefs.SetInt ("CarrotGem", CarrotGem - 10);
					PlayerPrefs.SetInt ("CrownGem", CrownGem - 10);
					PlayerPrefs.SetInt ("FireGem", FireGem - 10);
					PlayerPrefs.SetInt ("Picatsso", 1);
					PlayerPrefs.Save ();
					StartCoroutine(Fading ());
					StartCoroutine(Wait ());
					//SceneManager.LoadScene ("HUB");
				}
			}

		}
	} 



	/*
	 * aqui en CHANGEPOSITION podemos agregar un IF a cada uno de los IF que ya está
	 * para decir:
	 * SI ESTE PERSONAJE YA SE DESBLOQUEO, ACTUALPOS PARA B DRAGON
	 * Y HIDEPOS PARA L DRAGON
	 * Y lo mismo al reves
	 * 
	 * 
	 * NOTA: los que empiezan con B de BOARD son las descripciones
	 * los que empiezan con L de LOCKED son las gemas necesarias
	 * 
	 * JORGE LO TERMINA**********
	 */

public void changePosition(int changeTo){
		if (changeTo==1) {//BAG
			GameObject.Find("B_bag").transform.position=this.actualPos;

			GameObject.Find("B_zombie").transform.position=this.hidePos;
			GameObject.Find("B_bunny").transform.position=this.hidePos;
			GameObject.Find("B_puff").transform.position=this.hidePos;
			GameObject.Find("B_dragon").transform.position=this.hidePos;
			GameObject.Find("B_cat").transform.position=this.hidePos;

			GameObject.Find("L_zombie").transform.position=this.hidePos;
			GameObject.Find("L_bunny").transform.position=this.hidePos;
			GameObject.Find("L_puff").transform.position=this.hidePos;
			GameObject.Find("L_dragon").transform.position=this.hidePos;
			GameObject.Find("L_cat").transform.position=this.hidePos;
			playSound (0);
			transform.position=this.pos01;
		}
		if (changeTo==2) {//ZOMBIE
			if (PlayerPrefs.GetInt("WalkingZombie")==1) { // SI ESTÁ DESBLOQUEADO
				GameObject.Find("B_zombie").transform.position=this.actualPos;
				GameObject.Find("L_zombie").transform.position=this.hidePos;
			}else{ //SI NO ESTA DESBLOQUEADO MUESTRA GEMAS NECESARIAS
				GameObject.Find("B_zombie").transform.position=this.hidePos;
				GameObject.Find("L_zombie").transform.position=this.actualPos;
			}

			GameObject.Find("B_bag").transform.position=this.hidePos;
			GameObject.Find("B_bunny").transform.position=this.hidePos;
			GameObject.Find("B_puff").transform.position=this.hidePos;
			GameObject.Find("B_dragon").transform.position=this.hidePos;
			GameObject.Find("B_cat").transform.position=this.hidePos;

			GameObject.Find("L_bunny").transform.position=this.hidePos;
			GameObject.Find("L_puff").transform.position=this.hidePos;
			GameObject.Find("L_dragon").transform.position=this.hidePos;
			GameObject.Find("L_cat").transform.position=this.hidePos;

			playSound (0);
			transform.position=this.pos02;
		}
		if (changeTo==3) {//BUNNY

			if (PlayerPrefs.GetInt("MadBunny")==1) { // SI ESTÁ DESBLOQUEADO
				GameObject.Find("B_bunny").transform.position=this.actualPos;
				GameObject.Find("L_bunny").transform.position=this.hidePos;
			}else{ //SI NO ESTA DESBLOQUEADO MUESTRA GEMAS NECESARIAS
				GameObject.Find("B_bunny").transform.position=this.hidePos;
				GameObject.Find("L_bunny").transform.position=this.actualPos;
			}


			GameObject.Find("B_bag").transform.position=this.hidePos;
			GameObject.Find("B_zombie").transform.position=this.hidePos;
			GameObject.Find("B_puff").transform.position=this.hidePos;
			GameObject.Find("B_dragon").transform.position=this.hidePos;
			GameObject.Find("B_cat").transform.position=this.hidePos;

			GameObject.Find("L_zombie").transform.position=this.hidePos;
			GameObject.Find("L_puff").transform.position=this.hidePos;
			GameObject.Find("L_dragon").transform.position=this.hidePos;
			GameObject.Find("L_cat").transform.position=this.hidePos;


			playSound (0);
			transform.position=this.pos03;
		}
		if (changeTo==4) {//PUFF

			if (PlayerPrefs.GetInt("PrincessPuff")==1) { // SI ESTÁ DESBLOQUEADO
				GameObject.Find("B_puff").transform.position=this.actualPos;
				GameObject.Find("L_puff").transform.position=this.hidePos;
			}else{ //SI NO ESTA DESBLOQUEADO MUESTRA GEMAS NECESARIAS
				GameObject.Find("B_puff").transform.position=this.hidePos;
				GameObject.Find("L_puff").transform.position=this.actualPos;
			}

			GameObject.Find("B_bag").transform.position=this.hidePos;
			GameObject.Find("B_zombie").transform.position=this.hidePos;
			GameObject.Find("B_bunny").transform.position=this.hidePos;
			GameObject.Find("B_dragon").transform.position=this.hidePos;
			GameObject.Find("B_cat").transform.position=this.hidePos;

			GameObject.Find("L_zombie").transform.position=this.hidePos;
			GameObject.Find("L_bunny").transform.position=this.hidePos;
			GameObject.Find("L_dragon").transform.position=this.hidePos;
			GameObject.Find("L_cat").transform.position=this.hidePos;

			playSound (0);
			transform.position=this.pos04;
		}
		if (changeTo==5) {//DRAGON
			if (PlayerPrefs.GetInt("Dragon")==1) { // SI ESTÁ DESBLOQUEADO
				GameObject.Find("B_dragon").transform.position=this.actualPos;
				GameObject.Find("L_dragon").transform.position=this.hidePos;
			}else{ //SI NO ESTA DESBLOQUEADO MUESTRA GEMAS NECESARIAS
				GameObject.Find("B_dragon").transform.position=this.hidePos;
				GameObject.Find("L_dragon").transform.position=this.actualPos;
			}
			GameObject.Find("B_bag").transform.position=this.hidePos;
			GameObject.Find("B_zombie").transform.position=this.hidePos;
			GameObject.Find("B_bunny").transform.position=this.hidePos;
			GameObject.Find("B_puff").transform.position=this.hidePos;
			GameObject.Find("B_cat").transform.position=this.hidePos;

			GameObject.Find("L_zombie").transform.position=this.hidePos;
			GameObject.Find("L_bunny").transform.position=this.hidePos;
			GameObject.Find("L_puff").transform.position=this.hidePos;
			GameObject.Find("L_cat").transform.position=this.hidePos;

			playSound (0);
			transform.position=this.pos05;
		}
		if (changeTo==6) {//CAT
			if (PlayerPrefs.GetInt("Picatsso")==1) { // SI ESTÁ DESBLOQUEADO
				GameObject.Find("B_cat").transform.position=this.actualPos;
				GameObject.Find("L_cat").transform.position=this.hidePos;
			}else{ //SI NO ESTA DESBLOQUEADO MUESTRA GEMAS NECESARIAS
				GameObject.Find("B_cat").transform.position=this.hidePos;
				GameObject.Find("L_cat").transform.position=this.actualPos;
			}
			GameObject.Find("B_bag").transform.position=this.hidePos;
			GameObject.Find("B_zombie").transform.position=this.hidePos;
			GameObject.Find("B_bunny").transform.position=this.hidePos;
			GameObject.Find("B_puff").transform.position=this.hidePos;
			GameObject.Find("B_dragon").transform.position=this.hidePos;

			GameObject.Find("L_zombie").transform.position=this.hidePos;
			GameObject.Find("L_bunny").transform.position=this.hidePos;
			GameObject.Find("L_puff").transform.position=this.hidePos;
			GameObject.Find("L_dragon").transform.position=this.hidePos;

			playSound (0);
			transform.position=this.pos06;
		}

	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}