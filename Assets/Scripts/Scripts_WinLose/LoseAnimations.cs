﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseAnimations : MonoBehaviour {

	//Variable para animaciones 
	private Animator anim;

	public AudioClip[] audioClip;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		playSound(0);
		this.anim = this.GetComponent<Animator> ();
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*this.anim.Play ("Default_Lost");
		this.anim.Play ("Zombie_Lost");
		this.anim.Play ("Bunny_Lost");
		this.anim.Play ("Puff_Lost");
		this.anim.Play ("Dragon_Lost");
		this.anim.Play ("Raccstar_Lost");
		this.anim.Play ("Picatsso_Lost");*/
		anim.SetBool ("ItLost", true);
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}
}
