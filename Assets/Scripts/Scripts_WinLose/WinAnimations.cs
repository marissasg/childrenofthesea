﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinAnimations : MonoBehaviour {

	//Variable para animaciones 
	private Animator anim;

	public AudioClip[] audioClip;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		playSound(0);
		this.anim = this.GetComponent<Animator> ();
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*this.anim.Play ("Default_Win");
		this.anim.Play ("Zombie_Win");
		this.anim.Play ("Bunny_Win");
		this.anim.Play ("Puff_Win");
		this.anim.Play ("Dragon_Win");
		this.anim.Play ("Picatsso_Win");
		this.anim.Play ("Raccstar_Win");*/
		anim.SetBool ("ItWin", true);
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}
