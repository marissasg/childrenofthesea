﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLose_gems : MonoBehaviour {

	public Text gems = null;
	public Image image;

	void Start () {
		if (PlayerPrefs.GetInt ("CharacterID") == 1) {
			this.gems.text = PlayerPrefs.GetInt ("CoralGem").ToString();
			this.image.sprite=Resources.Load<Sprite> ("coral_");
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 2) {
			this.gems.text = PlayerPrefs.GetInt ("BoneGem").ToString();
			this.image.sprite=Resources.Load<Sprite> ("Bone_");
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 3) {
			this.gems.text = PlayerPrefs.GetInt ("CarrotGem").ToString();
			this.image.sprite=Resources.Load<Sprite> ("zanahoria_");
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 4) {
			this.gems.text = PlayerPrefs.GetInt ("CrownGem").ToString();
			this.image.sprite=Resources.Load<Sprite> ("corona_gris_");
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 5) {
			this.gems.text = PlayerPrefs.GetInt ("FireGem").ToString();
			this.image.sprite=Resources.Load<Sprite> ("Fire_");
		}

	}

}
