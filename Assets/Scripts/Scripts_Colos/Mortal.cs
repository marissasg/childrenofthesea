﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Mortal : MonoBehaviour {

	public Image white;
	public Animator anim;

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Player"){
			StartCoroutine(Fading ());
		}
	}
		
	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		SceneManager.LoadScene("Lose");
		yield return new WaitUntil (()=>this.white.color.a==1);


	}
}
