﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//el que elige es el que se queda arriba, es solo un color 
public class FallingDown_GC : MonoBehaviour {
	public GameObject[] MovablePlataforms;
	private GameObject ChoosenPlatform;
	int choosen;

	//GABY
	/* COLORS
	 * 0 blue
	 * 1 deepblue
	 * 2 green
	 * 3 pink
	 * 4 purple
	 * 5 red
	 * 6 yellow
	 */

	//prueba
	//prueba2
	int player=5;//CAMBIAR ESTA VARIABLE AL JUGADOR REAL************************************************

	int random_color, random_gem, turns_;
	public Image Color;
	private Vector3 gemBlue= new Vector3 (-0.848f, 2.5f, 2.05f);
	private Vector3 gemPink= new Vector3 (-0.026f, 2.5f, 2.561f);
	private Vector3 gemGreen= new Vector3 (0.82f, 2.5f, 2.056f);
	private Vector3 gemDeepBlue= new Vector3 (0.826f, 2.5f, 1.083f);
	private Vector3 gemPurple= new Vector3 (-0.014f, 2.5f, 0.612f);
	private Vector3 gemRed= new Vector3 (-0.873f, 2.5f, 1.089f);
	private Vector3 gemYellow= new Vector3 (-0.02f, 2.5f, 1.573f);

	public GameObject gemCoral, gemBone, gemCrown, gemCarrot, gemFire, gemFish;
	GameObject gem, gemPlayer, findGem;

	[SerializeField]
	public Text turns = null;

	private float secondsF = 4.0f; //***********************AQUI SE INICIALIZA EN CUANTO VA A EMPEZAR EL JUEGO

	void Start () {
		this.turns_ = 0;

		//asignar a GEMPLAYER la gema del jugador actual, aqui hay que igar la variable a Player prefs i guess
		switch(player) {
		case 0: //MAPACHE
			this.gemPlayer = gemCoral;
			break;

		case 1: //ZOMBIE
			this.gemPlayer = gemBone;
			break;

		case 2: // BUNNY
			this.gemPlayer = gemCarrot;
			break;

		case 5: //CAT
			this.gemPlayer = gemFish;
			break;
		}

		StartCoroutine ("StartWait");
	}

	IEnumerator WaitAmoment(){
		this.turns_+=1;
		this.turns.text = this.turns_.ToString ();

		yield return new WaitForSeconds (2); //**********************ESTE ES EL TIEMPO QUE ESPERA ANTES DE SACAR UN NUEVO COLOR
		StartCoroutine ("StartWait");
	}

	IEnumerator StartWait(){
		random_color = Random.Range (0,7);
		random_gem = Random.Range (0,7);

		changeColorImage ();
		instantiateGem ();

		if(this.secondsF>1.0f){
			this.secondsF -= 0.20f; //*************************AQUI VA RESTANDO CADA TURNO 
		}else{
			this.secondsF = 1.0f; //*****************CUANDO LLEGUE A ESTA CONDICION, SIEMPRE SE QUEDARA EN ESTE NUMERO
		}
		print (this.secondsF);

		yield return new WaitForSeconds (this.secondsF);

		GameInitialize ();
	}

	void GameInitialize(){
		
		ChoosenPlatform = MovablePlataforms [random_color];

		this.Color.sprite=Resources.Load<Sprite> ("clearImage");

		foreach(GameObject movablePlatform in MovablePlataforms){
			if(movablePlatform!=ChoosenPlatform){
				movablePlatform.GetComponent<PlatformMovement>().AllowToMove = true;
			}
		}
		destroyGems ();

		StartCoroutine ("WaitAmoment");
		//GameInitialize ();
	}

	void changeColorImage () {
		//CAMBIAR LA IMAGEN SEGUN EL COLOR RANDOM
		switch(random_color) {
		case 0:
			this.Color.sprite=Resources.Load<Sprite> ("color_blue");
			break;

		case 1:
			this.Color.sprite=Resources.Load<Sprite> ("color_deepblue");
			break;

		case 2:
			this.Color.sprite=Resources.Load<Sprite> ("color_green");
			break;

		case 3:
			this.Color.sprite=Resources.Load<Sprite> ("color_pink");
			break;

		case 4:
			this.Color.sprite=Resources.Load<Sprite> ("color_purple");
			break;

		case 5:
			this.Color.sprite=Resources.Load<Sprite> ("color_red");
			break;

		case 6:
			this.Color.sprite=Resources.Load<Sprite> ("color_yellow");
			break;
		}


	}

	void instantiateGem () {
		//INSTANCIAR LA GEMA DEL JUGADOR ACTUAL EN LA POSICION RANDOM
		switch(random_gem) {
		case 0:
			this.gem = Instantiate (this.gemPlayer, this.gemBlue, Quaternion.identity) as GameObject;
			break;

		case 1:
			this.gem = Instantiate (this.gemPlayer, this.gemDeepBlue, Quaternion.identity) as GameObject;
			break;

		case 2:
			this.gem = Instantiate (this.gemPlayer, this.gemGreen, Quaternion.identity) as GameObject;
			break;

		case 3:
			this.gem = Instantiate (this.gemPlayer, this.gemPink, Quaternion.identity) as GameObject;
			break;

		case 4:
			this.gem = Instantiate (this.gemPlayer, this.gemPurple, Quaternion.identity) as GameObject;
			break;

		case 5:
			this.gem = Instantiate (this.gemPlayer, this.gemRed, Quaternion.identity) as GameObject;
			break;

		case 6:
			this.gem = Instantiate (this.gemPlayer, this.gemYellow, Quaternion.identity) as GameObject;
			break;
		}

	}

	void destroyGems(){

		//DESTRUIR LAS INSTANCIAS
		switch(this.player) {
		case 0: //MAPACHE
			this.findGem=GameObject.FindWithTag ("Gem_Coral");
			Destroy (findGem);
			break;

		case 1: // ZOMBIE
			this.findGem=GameObject.FindWithTag ("Gem_Bone");
			Destroy (findGem);
			break;

		case 2: // MAD BUNNY
			this.findGem=GameObject.FindWithTag ("Gem_Carrot");
			Destroy (findGem);
			break;

		case 3: //PUFF
			this.findGem=GameObject.FindWithTag ("Gem_Crown");
			Destroy (findGem);
			break;

		case 4: //DRAGON
			this.findGem=GameObject.FindWithTag ("Gem_Fire");
			Destroy (findGem);
			break;

		case 5: //CAT
			this.findGem=GameObject.FindWithTag ("Gem_Fish");
			Destroy (findGem);
			break;

		}





	}

}
