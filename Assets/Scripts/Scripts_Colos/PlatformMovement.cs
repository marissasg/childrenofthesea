﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement: MonoBehaviour {
	bool isColliding;
	public bool AllowToMove, up;
	float speed;
	Vector3 PositionOriginal;
	Vector3 tempPos;

//TO DO: Hacer los waits
	void Start () {
		isColliding = false;
		AllowToMove = false;
		up = false;
		speed = 0.7f;
		PositionOriginal = transform.position;
		this.tempPos = transform.position;
	}

	void Update () {

		if (AllowToMove==true){
			if (up==false){
				DownPlatforms();
			}else{
				UpPlatforms();
			}
		}

	}


	void DownPlatforms(){
		if(this.up==false && this.transform.position.y>-0.6f){ //BAJAR
			this.tempPos = transform.position;
			tempPos.y -= 0.10f;
			transform.position = this.tempPos;
			//print ("BAJE");
		}else{
			this.up = true;
			//print ("up es true");
		}
			
	}
		
	void UpPlatforms(){

		//print ("ENTRE A SUBIR");

		if(this.up==true && this.transform.position.y<1.34f){ //SUBIR
				this.tempPos = transform.position;
			tempPos.y += 0.10f;
				transform.position = this.tempPos;
				//print ("SUBI");
			}else{
				this.up = false;
				this.AllowToMove = false;
				//print ("up es false");
			}
	
	}
		
}
