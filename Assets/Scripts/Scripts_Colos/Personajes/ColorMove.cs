﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorMove : MonoBehaviour {

	Rigidbody RB;
	private float PlayerSpeed;
	private Animator anim;

	//Parámetros para el salto 
	bool grounded = false; 
	Collider[] groundCollisions;
	float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	public Transform groundCheck;
	public float jumpHeight;

	void Start(){
		RB = GetComponent<Rigidbody> ();
		this.anim = this.GetComponent<Animator> ();
		if(PlayerPrefs.GetInt("CharacterID")==5) {
			this.PlayerSpeed = 3.0f;
		}else {
			this.PlayerSpeed = 1.5f;
		}
	}

	void Update(){

		//Salto
		if (grounded && Input.GetButton ("Jump")) {
			grounded = false;
			anim.SetBool ("Grounded", grounded);
			//anim.SetFloat("verticalSpeed", RB.velocity.y);
			RB.AddForce (new Vector3 (0, jumpHeight, 0));
		}

		groundCollisions = Physics.OverlapSphere (groundCheck.position, groundCheckRadius, groundLayer);
		if (groundCollisions.Length > 0) {
			grounded = true;

		} else {
			grounded = false;
		}


		anim.SetBool ("isInHUB", false);
		anim.SetBool ("isInMaze",false);
		anim.SetBool ("isSeq", false);
		anim.SetBool ("ItLost", false);
		anim.SetBool ("ItWin", false);
		anim.SetBool ("Grounded", grounded);
		anim.SetFloat("verticalSpeed", RB.velocity.y);

		//Movimiento de joystick
		float move = (Input.GetAxis ("Horizontal")) + (Input.GetAxis("Vertical"));
		anim.SetFloat ("speed", Mathf.Abs (move));

		if (Input.GetAxis ("Horizontal") > 0) { //Derecha
			transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Horizontal") < 0) { //Izquierda
			transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (-90f, 0f, 0f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Vertical") > 0) { //Arriba
			transform.Translate (Vector3.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (0f, 0f, 90f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
		if (Input.GetAxis ("Vertical") < 0) { //Abajo
			transform.Translate (Vector3.forward * Input.GetAxis ("Vertical") * PlayerSpeed * Time.deltaTime, Space.World);
			Vector3 targetPoint = new Vector3 (0f, 0f, -90f);
			var targetRot = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot ,0.2f);
		}
	}
}
