﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomColor : MonoBehaviour {

		/* COLORS
	 * 0 blue
	 * 1 deepblue
	 * 2 green
	 * 3 pink
	 * 4 purple
	 * 5 red
	 * 6 yellow
	 */

	int player=0;//CAMBIAR ESTA VARIABLE AL JUGADOR REAL************************************************

	int random_color, random_gem;
	public Image Color;
	private Vector3 gemBlue= new Vector3 (-1.008f, 2.942f, 2.04f);
	private Vector3 gemDeepBlue= new Vector3 (0.88f, 2.942f, 0.91f);
	private Vector3 gemGreen= new Vector3 (0.99f, 2.942f,1.99f);
	private Vector3 gemPink= new Vector3 (-0.05f, 2.942f, 2.52f);
	private Vector3 gemPurple= new Vector3 (-0.05f, 2.942f, 0.42f);
	private Vector3 gemRed= new Vector3 (-1.06f, 2.942f, 0.94f);
	private Vector3 gemYellow= new Vector3 (-0.05f, 2.942f, 0.42f);

	public GameObject gemCoral, gemBone, gemCrown, gemCarrot, gemFire, gemFish;
	GameObject gem, gemPlayer;


	void Start () {
		//asignar a GEMPLAYER la gema del jugador actual, aqui hay que igar la variable a layer prefs i guess
		switch(player) {
		case 0: //MAPACHE
			this.gemPlayer = gemCoral;
			break;

		case 1: //ZOMBIE
			this.gemPlayer = gemBone;
			break;

		case 2: // BUNNY
			this.gemPlayer = gemCarrot;
			break;

		case 3: //PUFF
			this.gemPlayer = gemCrown;
			break;

		case 4://DRAGON
			this.gemPlayer = gemFire;
			break;

		case 5: //CAT
			this.gemPlayer = gemFish;
			break;
		}

		StartCoroutine ("StartWait");
	}

	void Update () {
		
	}


	IEnumerator StartWait(){
		yield return new WaitForSeconds (3);
		//generar numeros random, uno para elegir un color y el otro para la posicion de la gema
		random_color = Random.Range (0,7);
		random_gem = Random.Range (0,7);

		//impresiones prueba
		//print (random_color);
		//print (random_gem);

		changeColorImage ();
		instantiateGem ();

		StartCoroutine ("SecondsToFall");

	}

	IEnumerator SecondsToFall(){
		yield return new WaitForSeconds (3);
		//"BAJAN PLATAFORMAS"
	}
	void changeColorImage () {
		//CAMBIAR LA IMAGEN SEGUN EL COLOR RANDOM
		switch(random_color) {
		case 0:
			this.Color.sprite=Resources.Load<Sprite> ("color_blue");
			break;

		case 1:
			this.Color.sprite=Resources.Load<Sprite> ("color_deepblue");
			break;

		case 2:
			this.Color.sprite=Resources.Load<Sprite> ("color_green");
			break;

		case 3:
			this.Color.sprite=Resources.Load<Sprite> ("color_pink");
			break;

		case 4:
			this.Color.sprite=Resources.Load<Sprite> ("color_purple");
			break;

		case 5:
			this.Color.sprite=Resources.Load<Sprite> ("color_red");
			break;

		case 6:
			this.Color.sprite=Resources.Load<Sprite> ("color_yellow");
			break;
		}


	}

	void instantiateGem () {

		//INSTANCIAR LA GEMA DEL JUGADOR ACTUAL EN LA POSICION RANDOM
		switch(random_color) {
		case 0:
			this.gem = Instantiate (this.gemPlayer, this.gemBlue, Quaternion.identity) as GameObject;
			break;

		case 1:
			this.gem = Instantiate (this.gemPlayer, this.gemDeepBlue, Quaternion.identity) as GameObject;
			break;

		case 2:
			this.gem = Instantiate (this.gemPlayer, this.gemGreen, Quaternion.identity) as GameObject;
			break;

		case 3:
			this.gem = Instantiate (this.gemPlayer, this.gemPink, Quaternion.identity) as GameObject;
			break;

		case 4:
			this.gem = Instantiate (this.gemPlayer, this.gemPurple, Quaternion.identity) as GameObject;
			break;

		case 5:
			this.gem = Instantiate (this.gemPlayer, this.gemRed, Quaternion.identity) as GameObject;
			break;

		case 6:
			this.gem = Instantiate (this.gemPlayer, this.gemYellow, Quaternion.identity) as GameObject;
			break;
		}

	}

}

