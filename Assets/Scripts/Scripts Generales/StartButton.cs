using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//FRIENDLY REMINDER:
//You can't write code if you use spanish variables

public class StartButton : MonoBehaviour {

	Scene scene;

	public Image white;
	public Animator anim;

void Start(){
        scene = SceneManager.GetActiveScene();
		Debug.Log("Active scene is '" + scene.name + "'.");
    }

void Update()
{
		if (Input.GetButton("Start") )
		{
			Select ();
			StartCoroutine(Fading ());

		}
}
	void Select()
	{
		
			if (this.scene.name == "Titles")
			{
				Invoke("HUB", 1.1f);
			}
			if (this.scene.name == "Ins_maze")
			{
				Invoke("Maze", 1.0f);
			}
			if (this.scene.name == "Ins_seq")
			{
				Invoke("Sequence", 1.0f);
			}
			if (this.scene.name == "Ins_plat")
			{
				Invoke("Platforms", 1.0f);
			}
			if (this.scene.name == "Ins_color")
			{
				Invoke("Color", 1.0f);
			}
			if (this.scene.name == "Lose")
			{
				Invoke("HUB", 1.0f);
			}
			if (this.scene.name == "Win")
			{
				Invoke("HUB", 1.0f);
			}
		}
	
	IEnumerator Fading(){
		anim.SetBool ("Fade", true);
		yield return new WaitUntil (()=>this.white.color.a==1);
	}

void HUB()
{
    SceneManager.LoadScene("HUB");
}

void Maze()
{
    SceneManager.LoadScene("Maze");
}

void Sequence()
{
    SceneManager.LoadScene("Sequence");
}

void Platforms()
{
    SceneManager.LoadScene("Plataformas");
}

void Color()
{
	SceneManager.LoadScene("FallingDown");
}



}