﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

	private float timer = 45;

	public Canvas pauseCanvas;

	public AudioClip[] audioClip;
	private AudioSource source;

	[SerializeField]
	public Text time_text=null;

	void Start () {
		//this.pauseCanvas = GetComponent<Canvas> ();
		this.pauseCanvas.enabled = false;
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Update () {
		timer -= Time.deltaTime;
		this.time_text.text = this.timer.ToString("f0");

		if(this.timer<0){
			Invoke ("Lose",1.0f);
		}

		if(Input.GetButton("Start")){
			if(Time.timeScale==1){
				playSound(0);
				this.pauseCanvas.enabled = true;
				Time.timeScale=0;
			}else{
				playSound(1);
				this.pauseCanvas.enabled = false;
				Time.timeScale=1;
			}
		}

	}

	void Lose(){
		SceneManager.LoadScene ("Lose");
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}
