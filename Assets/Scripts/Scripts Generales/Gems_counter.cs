﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gems_counter : MonoBehaviour {

	private int gemCoral, gemBone, gemCarrot, gemCrown, gemFire, gemFish;

	[SerializeField]
	public Text gemsCounter_text=null;

	public AudioClip[] audioClip;
	private AudioSource source;

	/*
	 * 	Bag		coral
	 * 	Zombie	bone
	 * 	Bunn	carrot
	 * 	Puff	crown
	 * 	Dragon	fire
	 *  Cat     Fish
	 */

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider col){

		if (col.gameObject.tag == "Gem_Coral"){
			this.gemCoral += 1;
			playSound(0);
			this.gemsCounter_text.text = this.gemCoral.ToString ();
			PlayerPrefs.SetInt ("CoralGem", PlayerPrefs.GetInt("CoralGem") + this.gemCoral);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "Gem_Bone"){
			this.gemBone += 1;
			playSound(0);
			//playSound(1);
			this.gemsCounter_text.text = this.gemBone.ToString ();
			PlayerPrefs.SetInt ("BoneGem", PlayerPrefs.GetInt("BoneGem") + this.gemBone);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "Gem_Carrot"){
			this.gemCarrot += 1;
			playSound(0);
			//playSound(1);
			this.gemsCounter_text.text = this.gemCarrot.ToString ();
			print (this.gemCarrot);
			PlayerPrefs.SetInt ("CarrotGem", PlayerPrefs.GetInt("CarrotGem") + this.gemCarrot);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "Gem_Crown"){
			this.gemCrown += 1;
			playSound(0);
			//playSound(1);
			this.gemsCounter_text.text = this.gemCrown.ToString ();
			PlayerPrefs.SetInt ("CrownGem", PlayerPrefs.GetInt("CrownGem") + this.gemCrown);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "Gem_Fire"){
			this.gemFire += 1;
			playSound(0);
			//playSound(1);
			this.gemsCounter_text.text = this.gemFire.ToString ();
			PlayerPrefs.SetInt ("FireGem", PlayerPrefs.GetInt("FireGem") + this.gemFire);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "Gem_Fish"){
			this.gemFish += 1;
			playSound(0);
			//playSound(1);
			this.gemsCounter_text.text = this.gemFish.ToString ();
			PlayerPrefs.SetInt ("FishGem", PlayerPrefs.GetInt("FishGem") + this.gemFish);
			PlayerPrefs.Save ();
		}

		if (col.gameObject.tag == "NextToDestroy"){
			//playSound(1);
		}
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}

}