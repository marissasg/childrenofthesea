﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gem_image : MonoBehaviour {

	public Image Gem_type;

	public GameObject global_variables;
	private Global_variables otherScriptToAccess;
	private int characterID_;

	/*
	 * 1	Bag
	 * 2	Zombie
	 * 3	Bunn
	 * 4	Puff
	 * 5	Dragon
	 * 6	Picatsso
	 */

	void Start () {

		this.otherScriptToAccess=global_variables.GetComponent<Global_variables> ();
		this.characterID_ = this.otherScriptToAccess.characterID;

		if(PlayerPrefs.GetInt("CharacterID")==1){
			this.Gem_type.sprite=Resources.Load<Sprite> ("coral_");
		}
		if(PlayerPrefs.GetInt("CharacterID")==2){
			this.Gem_type.sprite=Resources.Load<Sprite> ("Bone_");
		}
		if(PlayerPrefs.GetInt("CharacterID")==3 ){
			this.Gem_type.sprite=Resources.Load<Sprite> ("zanahoria_");
		}
		if(PlayerPrefs.GetInt("CharacterID")==4){
			this.Gem_type.sprite=Resources.Load<Sprite> ("corona_gris_");
		}
		if(PlayerPrefs.GetInt("CharacterID")==5){
			this.Gem_type.sprite=Resources.Load<Sprite> ("Fire_");
		}
		if(PlayerPrefs.GetInt("CharacterID")==6){
			this.Gem_type.sprite=Resources.Load<Sprite> ("Fish_"); 
		}
	}

}
