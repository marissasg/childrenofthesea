﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwapCharacter : MonoBehaviour {


	//ID de cada personaje 
	/*
		1 MAPACHEESTRELLA
		2 ZOMBIE
		3 BUNNY 
		4 POFF 
		5 DRAGONCERDO
		6 GATOPINTOR

	 * 
	 * */



	//Gameobjects de cada personaje
	public GameObject Bag; 
	public GameObject Zombie;
	public GameObject Bunny;
	public GameObject Puff;
	public GameObject Dragon;
	public GameObject Cat;

	//SCENE
	Scene scene;

	// Use this for initialization
	void Start () {
		scene = SceneManager.GetActiveScene ();

		//Cambio de Personaje dependiendo del Character ID
		if (PlayerPrefs.GetInt ("CharacterID") == 1) {
			if (this.scene.name == "Maze") {
				Bag.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "HUB") {
				Bag.transform.position = new Vector3 (.611f, 0.1300f, -.5f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Win") {
				Bag.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Lose") {
				Bag.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Sequence") {
				Bag.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Plataformas") {
				Bag.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "FallingDown") {
				Bag.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES BOLSA TIMIDA");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 2) {
			if (this.scene.name == "Maze") {
				Zombie.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "HUB") {
				Zombie.transform.position = new Vector3 (.611f, 0.1300f, -1.95f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Win") {
				Zombie.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Lose") {
				Zombie.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Sequence") {
				Zombie.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Plataformas") {
				Zombie.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "FallingDown") {
				Zombie.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES ZOMBIE FLOJO");
				Destroy (Bag);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 3) {
			if (this.scene.name == "Maze") {
				Bunny.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "HUB") {
				Bunny.transform.position = new Vector3 (.611f, 0.1300f, -1.95f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Win") {
				Bunny.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Lose") {
				Bunny.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Sequence") {
				Bunny.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Plataformas") {
				Bunny.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "FallingDown") {
				Bunny.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES LOLABONNI");
				Destroy (Zombie);
				Destroy (Bag);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Cat);
			}

		}
		if (PlayerPrefs.GetInt ("CharacterID") == 4) {
			if (this.scene.name == "Maze") {
				Puff.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "HUB") {
				Puff.transform.position = new Vector3 (.611f, 0.1300f, -1.95f);
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Win") {
				Puff.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Lose") {
				Puff.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Sequence") {
				Puff.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "Plataformas") {
				Puff.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES PRINCESS FAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
			if (this.scene.name == "FallingDown") {
				Puff.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES PRINCESSFAT");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Bag);
				Destroy (Dragon);
				Destroy (Cat);
			}
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 5) {
			if (this.scene.name == "Maze") {
				Dragon.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "HUB") {
				Dragon.transform.position = new Vector3 (.611f, 0.1300f, -1.95f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "Win") {
				Dragon.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "Lose") {
				Dragon.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "Sequence") {
				Dragon.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "Plataformas") {
				Dragon.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
			if (this.scene.name == "FallingDown") {
				Dragon.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES PUERCODRAGON");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Bag);
				Destroy (Cat);
			}
		}
		if (PlayerPrefs.GetInt ("CharacterID") == 6) {
			if (this.scene.name == "Maze") {
				Cat.transform.position = GameObject.Find ("PosPlayer(Clone)").transform.position;
				Destroy(GameObject.Find ("PosPlayer(Clone)"));
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "HUB") {
				Cat.transform.position = new Vector3 (.611f, 0.1300f, -1.95f);
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "Win") {
				Cat.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "Lose") {
				Cat.transform.position = new Vector3 (-4.69f, -0.598f, -1.25f);
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "Sequence") {
				Cat.transform.position = new Vector3 (-2.09f, -0.102f, -6.03f);
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "Plataformas") {
				Cat.transform.position = new Vector3 (0.6f, 5.5f, -0.5299f);
				print ("AHORA ERES PICATSSO");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
			if (this.scene.name == "FallingDown") {
				Cat.transform.position = new Vector3 (-0.06f, 2.534277f, 1.504867f);
				print ("AHORA ERES elpinchegatoese");
				Destroy (Zombie);
				Destroy (Bunny);
				Destroy (Puff);
				Destroy (Dragon);
				Destroy (Bag);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
