﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour {


	// Use this for initialization
	void Start () {
		//Variable para el cambio de personajes 
		/*
		 * 1 = Raccstar
		 * 2 = Zombie 
		 * 3 = MadBunny 
		 * 4 = Princess Puff 
		 * 5 = Dragon 
		 * 6 = Picatsso
		 * */
		PlayerPrefs.SetInt("CharacterID",1); //AQUI CAMBIAR EL PERSONAJE

		/*ESTAS MODIFICACIONES SE UNIRÁN AL CÓDIGO CUANDO YA ESTE LISTO TODO Y SE VAYA A MANDAR
		PlayerPrefs.SetInt ("CoralGem", 0);
		PlayerPrefs.SetInt ("BoneGem", 0);
		PlayerPrefs.SetInt ("CarrotGem", 0);
		PlayerPrefs.SetInt ("CrownGem", 0);
		PlayerPrefs.SetInt ("FireGem", 0);
		PlayerPrefs.SetInt ("FishGem", 0);
		PlayerPrefs.SetInt ("ShyBag", 1);
		PlayerPrefs.SetInt ("WalkingZombie", 0);
		PlayerPrefs.SetInt ("MadBunny", 0);
		PlayerPrefs.SetInt ("PrincessPuff", 0);
		PlayerPrefs.SetInt ("Dragon", 0);
		PlayerPrefs.SetInt ("Picatsso", 0);
		*/

		//Inicialización de variables para cada gema
		if (PlayerPrefs.HasKey ("CoralGem") && PlayerPrefs.HasKey ("BoneGem") && PlayerPrefs.HasKey ("CarrotGem") && PlayerPrefs.HasKey ("CrownGem") && PlayerPrefs.HasKey ("FireGem")) {
			print ("You have " + PlayerPrefs.GetInt ("CoralGem") + " Coral Gems");
			print ("You have " + PlayerPrefs.GetInt ("BoneGem") + " Bone Gems");
			print ("You have " + PlayerPrefs.GetInt ("CarrotGem") + " Carrot Gems");
			print ("You have " + PlayerPrefs.GetInt ("CrownGem") + " Crown Gems");
			print ("You have " + PlayerPrefs.GetInt ("FireGem") + " Fire Gems");
		} else {
			print ("You have " + PlayerPrefs.GetInt ("CoralGem") + " Coral Gems");
			print ("You have " + PlayerPrefs.GetInt ("BoneGem") + " Bone Gems");
			print ("You have " + PlayerPrefs.GetInt ("CarrotGem") + " Carrot Gems");
			print ("You have " + PlayerPrefs.GetInt ("CrownGem") + " Crown Gems");
			print ("You have " + PlayerPrefs.GetInt ("FireGem") + " Fire Gems");
			PlayerPrefs.SetInt ("CoralGem", 0);
			PlayerPrefs.SetInt ("BoneGem", 0);
			PlayerPrefs.SetInt ("CarrotGem", 0);
			PlayerPrefs.SetInt ("CrownGem", 0);
			PlayerPrefs.SetInt ("FireGem", 0);
			PlayerPrefs.SetInt ("FishGem", 0);
		}

		//Inicialización de variables para personajes desbloqueados 
		//Valor 1 = Personaje Desbloqueado
		//Valor 0 = Personaje Bloqueado
		if (PlayerPrefs.HasKey ("ShyBag") && PlayerPrefs.HasKey ("WalkingZombie") && PlayerPrefs.HasKey ("MadBunny") && PlayerPrefs.HasKey ("PrincessPuff") && PlayerPrefs.HasKey ("Dragon")) {
			if (PlayerPrefs.GetInt ("ShyBag") == 1) {
				print ("Character ShyBag Unlocked");
			} else {
				print ("Character ShyBag Locked");
			}

			if (PlayerPrefs.GetInt ("WalkingZombie") == 1) {
				print ("Character WalkingZombie Unlocked");
			} else {
				print ("Character WalkingZombie Locked");
			}

			if (PlayerPrefs.GetInt ("MadBunny") == 1) {
				print ("Character MadBunny Unlocked");
			} else {
				print ("Character MadBunny Locked");
			}

			if (PlayerPrefs.GetInt ("PrincessPuff") == 1) {
				print ("Character PrincessPuff Unlocked");
			} else {
				print ("Character PrincessPuff Locked");
			}

			if (PlayerPrefs.GetInt ("Dragon") == 1) {
				print ("Character Dragon Unlocked");
			} else {
				print ("Character Dragon Locked");
			}
		} else {
			PlayerPrefs.SetInt ("ShyBag", 1);
			PlayerPrefs.SetInt ("WalkingZombie", 0);
			PlayerPrefs.SetInt ("MadBunny", 0);
			PlayerPrefs.SetInt ("PrincessPuff", 0);
			PlayerPrefs.SetInt ("Dragon", 0);
			PlayerPrefs.SetInt ("Picatsso", 0);
		}

	}

	//HOLA JORGE

	// Update is called once per frame
	void Update () {
		
	}


}
