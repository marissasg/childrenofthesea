﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom_in : MonoBehaviour {
	public GameObject camera;

	private Vector3 startPos, endPos, aux;
	private float distance=30f;
	private float lerpTime=2f;
	private float currentLerpTime=0f;
	private bool moveNow=false;

	void Start () {
		this.startPos = camera.transform.position;
		this.aux.x = -5f;
		this.aux.y = 10f;
		this.aux.z = 55f;
		this.endPos = this.aux;
	}

	void Update () {
		if(Input.GetButton("Start")){
			this.moveNow=true;
		}
		if(this.moveNow==true){
			transform.Translate (this.endPos * Time.deltaTime*5);
		}
	}
		
}
