﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_ambiental : MonoBehaviour {
	public AudioClip[] audioClip;
	private AudioSource source;

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void Start () {
		playSound(0);
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}
}
