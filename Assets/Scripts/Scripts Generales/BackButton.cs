﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackButton : MonoBehaviour {

	public AudioClip[] audioClip;
	private AudioSource source;

	public Image white;
	public Animator anim;

	void Update () {
		if(Input.GetButton("Back")){
			playSound(0);
			StartCoroutine(Fading ());
			StartCoroutine(Wait ());
		}
	}

	IEnumerator Fading(){
		anim.SetBool ("Fade", true);

		yield return new WaitUntil (()=>this.white.color.a==1);
		//SceneManager.LoadScene("HUB");
	}

	IEnumerator Wait(){
		yield return new WaitForSeconds (0.9f);
		SceneManager.LoadScene("HUB");
	}

	void Awake() {
		source = GetComponent<AudioSource> ();
	}

	void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
	}


}
