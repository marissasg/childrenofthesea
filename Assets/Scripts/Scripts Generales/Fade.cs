﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fade : MonoBehaviour {

	public static Fade Instance { set; get;}

	public Image fadeImage;
	private bool isInTransition;
	private float transition;
	private bool isShowing;
	private float duration;

	Scene scene;

	private void Awake(){
		Instance = this;
	}

	public void Fading(bool showing, float duration){
		isShowing = showing;
		isInTransition = true;
		this.duration = duration;
		transition=(isShowing)?0:1;
	}

	private void Start(){
		scene = SceneManager.GetActiveScene();
		//Fading (false,0.9f);
	}
	private void Update(){
		
		//FADE OUT

		if(Input.GetButton("Start")){
			if(this.scene.name=="Titles"){
				//Fading (true,0.9f);
			}

		}

		if (!isInTransition)
			return;

		transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
		fadeImage.color = Color.Lerp (new Color(1,1,1,0),Color.black, transition);

		if (transition > 1 || transition < 0)
			isInTransition = false;
	}



}

















